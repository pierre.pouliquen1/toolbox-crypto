import unicodedata
import string
from collections import Counter

def remove_accents(input_text):
    nfkd_form = unicodedata.normalize('NFKD', input_text)
    return ''.join([c for c in nfkd_form if not unicodedata.combining(c)])

def remove_punctuation(input_text):
    translator = str.maketrans("", "", string.punctuation)
    return input_text.translate(translator)

def preprocess_text(input_text):
    text_without_accents = remove_accents(input_text)
    text_without_punctuation = remove_punctuation(text_without_accents)
    return text_without_punctuation.lower()

def calculate_letter_frequencies(processed_text):
    letter_count = Counter(processed_text)
    total_letters = sum(letter_count.values())
    letter_frequencies = {letter: count / total_letters for letter, count in letter_count.items()}
    return letter_frequencies

def replace_letters_by_frequencies(processed_text, letter_frequencies):
    sorted_frequencies = sorted(letter_frequencies.items(), key=lambda item: item[1], reverse=True)
    replacement_mapping = {sorted_frequencies[i][0]: 'ETAOINSHRDLUCMFWYPVBGKXJQZ'[i] for i in range(len(sorted_frequencies))}
    
    replaced_text = ''.join([replacement_mapping.get(letter, letter) for letter in processed_text])
    return replaced_text

def main():
    input_text = input("Enter a text: ")
    processed_text = preprocess_text(input_text)
    print("Processed text:", processed_text)

    letter_frequencies = calculate_letter_frequencies(processed_text)
    print("Letter frequencies:", letter_frequencies)

    replaced_text = replace_letters_by_frequencies(processed_text, letter_frequencies)
    print("Replaced text:", replaced_text)

if __name__ == "__main__":
    main()
