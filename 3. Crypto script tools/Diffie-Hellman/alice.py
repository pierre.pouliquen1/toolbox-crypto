import socket
import random

def diffie_hellman_private_key(p, g):
    return random.randint(2, p - 1)

def diffie_hellman_public_key(g, private_key, p):
    return (g ** private_key) % p

def calculate_shared_secret_key(their_public_key, my_private_key, p):
    return (their_public_key ** my_private_key) % p

def main():
    p = 23  # Prime number
    g = 5   # Generator

    private_key = diffie_hellman_private_key(p, g)
    public_key = diffie_hellman_public_key(g, private_key, p)

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(('localhost', 8888))

    sock.send(str(public_key).encode())
    their_public_key = int(sock.recv(1024).decode())

    shared_secret = calculate_shared_secret_key(their_public_key, private_key, p)

    print(f"Alice's private key: {private_key}")
    print(f"Alice's public key: {public_key}")
    print(f"Shared secret key: {shared_secret}")

    sock.close()

if __name__ == "__main__":
    main()
