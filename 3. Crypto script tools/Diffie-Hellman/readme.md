# Diffie-Hellman Key Exchange with Sockets

This repository contains two Python scripts, `alice.py` and `bob.py`, which demonstrate the Diffie-Hellman key exchange protocol using sockets. The Diffie-Hellman key exchange is a cryptographic method used to securely exchange encryption keys over an insecure channel.

## Prerequisites

- Python 3.x

## Usage

1. **Run Bob's Script (`bob.py`)**

   Open a terminal or command prompt and navigate to the directory containing the `bob.py` script. Run the following command:

   ```bash
   python bob.py
   ```

   Bob's script will bind to a local address and port (localhost:8888) and wait for incoming connections from Alice.

2. **Run Alice's Script (`alice.py`)**

   Open another terminal or command prompt and navigate to the directory containing the `alice.py` script. Run the following command:

   ```bash
   python alice.py
   ```

   Alice's script will connect to Bob's address and port, exchange public keys, and calculate the shared secret key.

3. **Observing Output**

   Both Alice's and Bob's scripts will display their private keys, public keys, and the calculated shared secret key. You can observe these outputs in their respective terminal or command prompt windows.

## How It Works

1. Bob and Alice each generate their own private key (`private_key`) and public key (`public_key`) based on the shared prime number (`p`) and generator (`g`).

2. Bob's script binds to a local address and port and waits for an incoming connection.

3. Alice's script connects to Bob's address and port, sending her public key.

4. Bob receives Alice's public key, calculates the shared secret key, and sends his own public key back to Alice.

5. Alice receives Bob's public key, calculates the shared secret key, and both Alice and Bob now have the same secret key without explicitly transmitting it over the network.

## Security Considerations

This implementation is for educational purposes and does not address all possible security concerns. In practice, you would need to consider factors such as ensuring secure socket connections, handling potential attacks like man-in-the-middle attacks, using larger prime numbers for better security, and incorporating additional cryptographic mechanisms.

## References

- [Diffie-Hellman Key Exchange](https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange)

---
