#!/bin/bash

# Function to detect and display certificates
detect_certificates() {
    # Find all files in the current directory and its subdirectories
    find . -type f | while read -r file; do
        # Check if the file is a certificate (assuming certificate files have extensions like .pem, .crt, .cer, .key, etc.)
        if [[ "$file" =~ \.pem$ || "$file" =~ \.crt$ || "$file" =~ \.cer$ || "$file" =~ \.key$ ]]; then
            # Display the certificate path
            echo "Certificate found: $file"
            
            # If you want to display the contents of the certificate, uncomment the following line
            openssl x509 -in "$file" -noout -text
        fi
    done
}

# Call the function to detect certificates
detect_certificates
