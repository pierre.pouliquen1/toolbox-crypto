# Certificate Detection Script

This Bash script searches for certificate files in the current directory and its subdirectories and displays their paths. Optionally, it can also display the contents of each certificate.

## Script Explanation

- `detect_certificates()`: Defines the function to search for certificate files (assumed to have extensions like `.pem`, `.crt`, `.cer`, `.key`, etc.) in the current directory and its subdirectories using the `find` command. For each file found, it checks if the file extension matches those of typical certificate files. If a match is found, it displays the path of the certificate file. Additionally, if you want to display the contents of the certificate, you can uncomment the `openssl x509` command to do so.
- The script calls the `detect_certificates` function to perform the certificate detection.

## Usage

1. Make sure you have Bash installed on your system.
2. Save the script to a file (e.g., `certificate_detection.sh`).
3. Make the script executable by running:
   ```
   chmod +x certificate_detection.sh
   ```
4. Run the script by executing:
   ```
   ./certificate_detection.sh
   ```

## Note

This script assumes that certificate files in the directory and its subdirectories have extensions like `.pem`, `.crt`, `.cer`, `.key`, etc. Adjust the file extensions as needed to match your certificate file formats. Additionally, be cautious when displaying the contents of certificates, especially in a production environment, as they may contain sensitive information.