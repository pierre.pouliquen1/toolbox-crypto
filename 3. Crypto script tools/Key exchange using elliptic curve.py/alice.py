import socket
from ecc_utils import point_multiply, point_add

def main():
    host = 'localhost'
    port = 12346

    a = 2
    b = 2
    p = 17
    base_point = (5, 1)
    private_key_A = 7

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((host, port))
        s.listen()

        print("Waiting for Bob to connect...")
        conn, addr = s.accept()
        print("Connected to Bob:", addr)

        public_key_A = point_multiply(base_point, private_key_A, a, p)
        public_key_A_str = f"{public_key_A[0]},{public_key_A[1]}"
        conn.send(public_key_A_str.encode())

        bob_public_key_str = conn.recv(1024).decode()
        bob_public_key_x, bob_public_key_y = map(int, bob_public_key_str.split(','))

        shared_secret = point_multiply((bob_public_key_x, bob_public_key_y), private_key_A, a, p)[0]
        print("Alice's shared secret:", shared_secret)

if __name__ == '__main__':
    main()
