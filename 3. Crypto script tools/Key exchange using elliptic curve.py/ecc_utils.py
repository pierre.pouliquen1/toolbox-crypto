def point_add(p, q, a, p_field):
    if p is None:
        return q
    if q is None:
        return p
    
    x1, y1 = p
    x2, y2 = q
    
    if p == q:
        m = (3 * x1 * x1 + a) * pow(2 * y1, -1, p_field) % p_field
    else:
        m = (y2 - y1) * pow(x2 - x1, -1, p_field) % p_field
    
    x3 = (m * m - x1 - x2) % p_field
    y3 = (m * (x1 - x3) - y1) % p_field
    
    return x3, y3

def point_multiply(p, scalar, a, p_field):
    result = None
    while scalar > 0:
        if scalar & 1:
            result = point_add(result, p, a, p_field)
        p = point_add(p, p, a, p_field)
        scalar >>= 1
    return result
