import socket
from ecc_utils import point_multiply, point_add

def main():
    host = 'localhost'
    port = 12346

    a = 2
    b = 2
    p = 17
    base_point = (5, 1)
    private_key_B = 13

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))

        alice_public_key_str = s.recv(1024).decode()
        alice_public_key_x, alice_public_key_y = map(int, alice_public_key_str.split(','))

        public_key_B = point_multiply(base_point, private_key_B, a, p)
        public_key_B_str = f"{public_key_B[0]},{public_key_B[1]}"
        s.send(public_key_B_str.encode())

        shared_secret = point_multiply((alice_public_key_x, alice_public_key_y), private_key_B, a, p)[0]
        print("Bob's shared secret:", shared_secret)

if __name__ == '__main__':
    main()
