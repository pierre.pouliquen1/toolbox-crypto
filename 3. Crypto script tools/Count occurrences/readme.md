# Letter Frequency Analysis

This script analyzes the frequency of letters in a given text, compares the frequencies with those in the French and English languages, and calculates a score to measure the difference between the text and the standard frequencies of these languages.

## Features

- Option to input a string directly or load text from a `text.txt` file.
- Removes accents from characters for more accurate frequency analysis.
- Displays the frequency of each character in the input text.
- Compares the character frequencies with standard frequencies in French and English.
- Calculates a score indicating the difference between the text's character frequencies and the standard frequencies in French and English.

## How to Use

1. **Install Python**: Ensure you have Python installed on your system. You can download it from [python.org](https://www.python.org/).

2. **Prepare the Script and Text File**:
    - Save the provided Python script as `letter_frequency.py`.
    - (Optional) Create a `text.txt` file in the same directory as the script if you choose to load text from a file.

3. **Run the Script**:
    - Open a terminal or command prompt.
    - Navigate to the directory where the script and `text.txt` file are located.
    - Run the script with the command:
      ```sh
      python letter_frequency.py
      ```

4. **Follow the Prompts**:
    - You will be prompted to choose between entering a string directly or loading from the `text.txt` file.
    - If you choose to enter a string, type your text and press Enter.
    - If you choose to load from `text.txt`, ensure the file exists and contains the text you want to analyze.

## Example

When you run the script, you will see an output similar to the following:

```
Do you want to enter a string (1) or load from text.txt (2)? 1
Enter a string: Hello World!

Character list: [' ', '!', 'd', 'e', 'h', 'l', 'o', 'r', 'w']
Occurrences: [1, 1, 1, 1, 1, 3, 2, 1, 1]
Percentages: [8.333333333333332, 8.333333333333332, 8.333333333333332, 8.333333333333332, 8.333333333333332, 25.0, 16.666666666666664, 8.333333333333332, 8.333333333333332]
Character list without accents: [' ', '!', 'd', 'e', 'h', 'l', 'o', 'r', 'w']
Occurrences without accents: [1, 1, 1, 1, 1, 3, 2, 1, 1]
Percentages without accents: [8.333333333333332, 8.333333333333332, 8.333333333333332, 8.333333333333332, 8.333333333333332, 25.0, 16.666666666666664, 8.333333333333332, 8.333333333333332]

Score compared to French frequencies: 132.73
Score compared to English frequencies: 117.67

Comparison with French frequencies:
a: 0.00% (Input) vs 7.64% (French)
b: 0.00% (Input) vs 0.90% (French)
c: 0.00% (Input) vs 3.26% (French)
...
y: 0.00% (Input) vs 0.13% (French)
z: 0.00% (Input) vs 0.33% (French)

Comparison with English frequencies:
a: 0.00% (Input) vs 8.17% (English)
b: 0.00% (Input) vs 1.49% (English)
c: 0.00% (Input) vs 2.78% (English)
...
y: 0.00% (Input) vs 1.97% (English)
z: 0.00% (Input) vs 0.07% (English)
```

## Notes

- The scores indicate how closely the text's letter frequencies match the standard frequencies for French and English. A lower score means a closer match.
- The script converts all characters to lowercase and removes accents to ensure accurate frequency analysis.
- Ensure your `text.txt` file is in UTF-8 encoding to avoid any reading errors.