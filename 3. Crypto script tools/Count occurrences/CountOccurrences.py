def remove_accents(string):
    accents = {
        'a': ['à', 'á', 'â', 'ã', 'ä', 'å'],
        'e': ['è', 'é', 'ê', 'ë'],
        'i': ['ì', 'í', 'î', 'ï'],
        'o': ['ò', 'ó', 'ô', 'õ', 'ö', 'ø'],
        'u': ['ù', 'ú', 'û', 'ü'],
        'y': ['ý', 'ÿ'],
        'c': ['ç'],
        'n': ['ñ']
    }

    for char, replacements in accents.items():
        for accented_char in replacements:
            string = string.replace(accented_char, char)

    return string

def character_occurrences(string):
    chars = list(string.lower())  # Convert to lowercase for consistency
    unique_chars = sorted(list(set(chars)))
    occurrences = [chars.count(char) for char in unique_chars]
    
    chars_without_accents = [remove_accents(char) for char in chars]
    unique_chars_without_accents = sorted(list(set(chars_without_accents)))
    occurrences_without_accents = [chars_without_accents.count(char) for char in unique_chars_without_accents]
    
    total_chars = len(chars)
    percentages = [occurrence / total_chars * 100 for occurrence in occurrences]
    total_chars_without_accents = len(chars_without_accents)
    percentages_without_accents = [occurrence / total_chars_without_accents * 100 for occurrence in occurrences_without_accents]
    
    return unique_chars, occurrences, percentages, unique_chars_without_accents, occurrences_without_accents, percentages_without_accents

def load_text_file(file_path):
    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            return file.read()
    except FileNotFoundError:
        print(f"File {file_path} not found.")
        return None

def main():
    import os

    choice = input("Do you want to enter a string (1) or load from text.txt (2)? ")

    if choice == '1':
        input_string = input("Enter a string: ")
    elif choice == '2':
        file_path = 'text.txt'
        if os.path.exists(file_path):
            input_string = load_text_file(file_path)
            if input_string is None:
                return
        else:
            print(f"File {file_path} does not exist.")
            return
    else:
        print("Invalid choice.")
        return

    chars, occurrences, percentages, chars_without_accents, occurrences_without_accents, percentages_without_accents = character_occurrences(input_string)

    # Frequencies of letters in French and English languages (source: Wikipedia)
    french_frequencies = {
        'a': 7.636, 'b': 0.901, 'c': 3.260, 'd': 3.669, 'e': 14.715,
        'f': 1.066, 'g': 0.866, 'h': 0.737, 'i': 7.529, 'j': 0.613,
        'k': 0.074, 'l': 5.456, 'm': 2.968, 'n': 7.095, 'o': 5.378,
        'p': 3.021, 'q': 1.362, 'r': 6.553, 's': 7.948, 't': 7.244,
        'u': 6.311, 'v': 1.838, 'w': 0.049, 'x': 0.427, 'y': 0.128, 'z': 0.326
    }
    
    english_frequencies = {
        'a': 8.167, 'b': 1.492, 'c': 2.782, 'd': 4.253, 'e': 12.702,
        'f': 2.228, 'g': 2.015, 'h': 6.094, 'i': 6.966, 'j': 0.153,
        'k': 0.772, 'l': 4.025, 'm': 2.406, 'n': 6.749, 'o': 7.507,
        'p': 1.929, 'q': 0.095, 'r': 5.987, 's': 6.327, 't': 9.056,
        'u': 2.758, 'v': 0.978, 'w': 2.360, 'x': 0.150, 'y': 1.974, 'z': 0.074
    }

    # Print the results
    print("Character list:", chars)
    print("Occurrences:", occurrences)
    print("Percentages:", percentages)
    print("Character list without accents:", chars_without_accents)
    print("Occurrences without accents:", occurrences_without_accents)
    print("Percentages without accents:", percentages_without_accents)
    
    # Comparisons with French and English frequencies
    def calculate_score(percentages, lang_frequencies):
        score = 0.0
        for char in lang_frequencies:
            if char in chars_without_accents:
                index = chars_without_accents.index(char)
                score += abs(percentages[index] - lang_frequencies[char])
            else:
                score += lang_frequencies[char]
        return score

    french_score = calculate_score(percentages_without_accents, french_frequencies)
    english_score = calculate_score(percentages_without_accents, english_frequencies)

    print(f"\nScore compared to French frequencies: {french_score:.2f}")
    print(f"Score compared to English frequencies: {english_score:.2f}")

    def print_comparison(percentages, lang_frequencies, lang_name):
        print(f"\nComparison with {lang_name} frequencies:")
        for char in sorted(lang_frequencies.keys()):
            if char in chars_without_accents:
                index = chars_without_accents.index(char)
                print(f"{char}: {percentages[index]:.2f}% (Input) vs {lang_frequencies[char]:.2f}% ({lang_name})")
            else:
                print(f"{char}: 0.00% (Input) vs {lang_frequencies[char]:.2f}% ({lang_name})")

    print_comparison(percentages_without_accents, french_frequencies, "French")
    print_comparison(percentages_without_accents, english_frequencies, "English")

if __name__ == "__main__":
    main()
