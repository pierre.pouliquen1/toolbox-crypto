# Markov Chain Monte Carlo Algorithm for Text Analysis

## Overview
This Python script implements a Markov Chain Monte Carlo (MCMC) algorithm for text analysis. The algorithm calculates the frequency of each letter appearing after another letter (including spaces) in a given text file. It then compares these frequencies between two different text files and calculates a weighted sum of the absolute differences. 

## Features
- Calculates the frequency of each letter appearing after another letter (including spaces) in a text file.
- Compares the frequency distributions between two text files.
- Computes a weighted sum of the absolute differences in frequency distributions.

## Usage
1. Ensure Python 3.x is installed on your system.
2. Clone this repository or download the Python script (`mcmc_text_analysis.py`) and the text files you want to analyze.
3. Run the Python script, providing the paths to the text files as arguments.

Example usage:
```
python mcmc_text_analysis.py
```

## Requirements
- Python 3.x

## File Descriptions
- `mcmc_text_analysis.py`: The main Python script implementing the MCMC algorithm.
- `base.txt`: Sample text file for analysis.
- `ciphered_text_analysis.txt`: Another sample text file for comparison.

## Author
This MCMC text analysis algorithm was developed by Pierre Pouliquen.

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

