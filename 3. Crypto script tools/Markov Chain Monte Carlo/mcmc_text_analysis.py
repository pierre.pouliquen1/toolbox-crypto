import unicodedata
import string

def remove_accents_and_punctuation(text):
    # Remove accents
    text = unicodedata.normalize('NFD', text).encode('ascii', 'ignore').decode("utf-8")

    # Remove punctuation and digits
    text = ''.join(char for char in text if char not in string.punctuation and not char.isdigit())

    return text

def calculate_frequencies(file):
    # Initialize a dictionary to store frequencies
    frequencies = {char: {char2: 0 for char2 in "abcdefghijklmnopqrstuvwxyz "} for char in "abcdefghijklmnopqrstuvwxyz "}

    # Initialize a dictionary to count the total occurrences of each letter
    total_letters = {char: 0 for char in "abcdefghijklmnopqrstuvwxyz "}

    with open(file, 'r') as f:
        text = f.read().lower()

    # Remove accents and punctuation
    text = remove_accents_and_punctuation(text)

    # Count the total occurrences of each letter in the text
    for char in text:
        if char in "abcdefghijklmnopqrstuvwxyz ":
            total_letters[char] += 1

    for i in range(len(text) - 1):
        char1 = text[i]
        char2 = text[i + 1]

        # Ignore newline and tab characters
        if char1 == '\n' or char2 == '\n' or char1 == '\t' or char2 == '\t':
            continue

        frequencies[char1][char2] += 1

    # Normalize frequencies with respect to the total occurrences of each letter
    for char1 in frequencies:
        total_letter = total_letters[char1]
        if total_letter != 0:  # Avoid division by zero
            for char2 in frequencies[char1]:
                frequencies[char1][char2] /= total_letter
                # Round the frequency to three decimal places
                frequencies[char1][char2] = round(frequencies[char1][char2], 3)

    return frequencies, total_letters

def calculate_absolute_differences(freq1, freq2):
    letters = "abcdefghijklmnopqrstuvwxyz "
    differences = []

    for char1 in letters:
        total_difference = 0
        for char2 in letters:
            total_difference += abs(freq1[char1][char2] - freq2[char1][char2])
        differences.append(total_difference)

    return differences

def weighted_sum_of_differences(differences, total_letters2):
    total_characters2 = sum(total_letters2.values())
    weighted_sum = 0

    for i, char in enumerate("abcdefghijklmnopqrstuvwxyz "):
        weighted_sum += differences[i] * total_letters2[char] / total_characters2

    return weighted_sum

# Example usage
frequencies1, _ = calculate_frequencies("base.txt")
frequencies2, total_letters2 = calculate_frequencies("ciphered-text.txt")
absolute_differences = calculate_absolute_differences(frequencies1, frequencies2)
weighted_sum = weighted_sum_of_differences(absolute_differences, total_letters2)

# Display the weighted sum of absolute differences
print(weighted_sum)
