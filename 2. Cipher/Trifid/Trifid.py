import math

trifidMatrix = [[[None for _ in range(3)] for _ in range(3)] for _ in range(3)]
pos = [(None, None, None) for _ in range(27)]

def trifid(key):
    found = [False] * 26
    it = 0

    for i in range(len(key)):
        if key[i] != ' ' and not found[ord(key[i]) - ord('A')]:
            trifidMatrix[it // 9][(it % 9) // 3][(it % 9) % 3] = key[i]
            found[ord(key[i]) - ord('A')] = True
            pos[ord(key[i]) - ord('A')] = (it // 9, (it % 9) // 3, (it % 9) % 3)
            it += 1

    for i in range(26):
        if not found[i]:
            trifidMatrix[it // 9][(it % 9) // 3][(it % 9) % 3] = chr(i + ord('A'))
            pos[i] = (it // 9, (it % 9) // 3, (it % 9) % 3)
            it += 1

    trifidMatrix[2][2][2] = '+'
    pos[26] = (2, 2, 2)

def encrypt(text):
    matrix = [[0 for _ in range(100)] for _ in range(3)]
    result = ""

    for i in range(len(text)):
        x1, x2, x3 = pos[ord(text[i]) - ord('A')]
        matrix[0][i] = x1
        matrix[1][i] = x2
        matrix[2][i] = x3

    for i in range(len(text) // 5):
        it = 0
        for j in range(i * 5, i * 5 + 5):
            x1 = matrix[it // 5][i * 5 + it % 5]
            it += 1
            x2 = matrix[it // 5][i * 5 + it % 5]
            it += 1
            x3 = matrix[it // 5][i * 5 + it % 5]
            it += 1
            result += trifidMatrix[x1][x2][x3]

    return result

def decrypt(text):
    result = ""
    matrix = [[0 for _ in range(100)] for _ in range(3)]
    it = 0

    for i in range(len(text)):
        if text[i] != '+':
            x1, x2, x3 = pos[ord(text[i]) - ord('A')]
        else:
            x1 = x2 = x3 = 2

        matrix[it // 5][(i // 5) * 5 + it % 5] = x1
        it += 1
        matrix[it // 5][(i // 5) * 5 + it % 5] = x2
        it += 1
        matrix[it // 5][(i // 5) * 5 + it % 5] = x3
        it += 1
        it %= 15

    for i in range(len(text)):
        x1 = matrix[0][i]
        x2 = matrix[1][i]
        x3 = matrix[2][i]
        result += trifidMatrix[x1][x2][x3]

    return result

def main():
    text = input("Plaintext: ")
    text = text.upper().replace('J', 'I').replace(' ', '')
    
    key = input("Key: ")
    key = key.upper().replace(' ', '')

    trifid(key)
    cipher = encrypt(text)
    print("Ciphertext:", cipher)
    print("Result:", decrypt(cipher))

if __name__ == "__main__":
    main()
