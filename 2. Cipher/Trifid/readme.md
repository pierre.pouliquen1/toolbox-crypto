# Trifid Cipher

This Python script implements the Trifid cipher, which is a polygraphic substitution cipher. The Trifid cipher encrypts letters by converting them into their corresponding coordinates in a 3x3x3 grid, and then rearranging the coordinates according to a key.

## Functions

### `trifid(key)`
Generates the Trifid matrix and position lookup table based on the given key.

### `encrypt(text)`
Encrypts the plaintext using the Trifid cipher.

### `decrypt(text)`
Decrypts the ciphertext using the Trifid cipher.

### `main()`
The main function where the user can input the plaintext and the key, and the script prints the resulting ciphertext and the decrypted plaintext.

## Usage

1. Run the script.
2. Enter the plaintext and the key when prompted.
3. The script will print the ciphertext and the decrypted plaintext.

## Example

```
Plaintext: Hello World
Key: CIPHERKEY
Ciphertext: BEHDCGADBFBEEDF
Result: HELLOWORLD
```

In this example, the plaintext "Hello World" is encrypted using the Trifid cipher with the key "CIPHERKEY". The resulting ciphertext is "BEHDCGADBFBEEDF", and decrypting it using the same key yields the original plaintext "HELLOWORLD".

Feel free to use this script to encrypt and decrypt messages using the Trifid cipher!