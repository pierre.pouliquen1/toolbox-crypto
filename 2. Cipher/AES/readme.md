**AES Encryption and Decryption Scripts README**

This repository contains Python scripts for performing AES encryption and decryption using various AES operations. AES (Advanced Encryption Standard) is a widely used symmetric encryption algorithm that provides strong security for data confidentiality. The repository includes the following scripts:

1. `aes_operations.py`: This script defines various AES operations such as SubBytes, ShiftRows, MixColumns, AddRoundKey, and their inverse transformations. It also includes functions for Galois field addition and multiplication, which are fundamental operations in AES. Example usage is provided at the end of the script.

2. `aes_example.py`: This script showcases how to use the `Crypto.Cipher` library to perform AES encryption and decryption. It includes functions for padding and unpadding messages, encrypting using the ECB (Electronic Codebook) mode, and decrypting the encrypted ciphertext. The `main` function demonstrates the encryption and decryption process using a randomly generated key and a sample plaintext.

**Usage**

1. Clone the repository to your local machine:


2. Run the `AES no library.py` script to understand the AES operations and transformations:

```
python aes_operations.py
```

This script defines and demonstrates AES operations like SubBytes, ShiftRows, MixColumns, and AddRoundKey, as well as their inverse transformations. It should be noted that there is only one round that is carried out and that it only works with 4 bits since it is just an educational example

3. Run the `AES with library.py` script to see how AES encryption and decryption are performed:

```
python aes_example.py
```

This script showcases AES encryption and decryption using the `Crypto.Cipher` library. It generates a random 128-bit key, encrypts a sample plaintext, and then decrypts the ciphertext to retrieve the original message.

**Important Note**

- These scripts are for educational purposes and demonstrate basic AES operations. In a real-world scenario, it is recommended to use established and well-tested cryptographic libraries and practices to ensure secure encryption and decryption.

**Requirements**

- Python 3.x
- `Crypto.Cipher` library (you can install it using `pip install pycryptodome`)