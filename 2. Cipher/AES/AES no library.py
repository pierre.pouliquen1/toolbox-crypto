# Substitution tables S-box for SubBytes
s_box = [
    [0x0, 0x3, 0x6, 0x5, 0xC, 0xA, 0xD, 0xE, 0x8, 0x4, 0x7, 0x1, 0xF, 0x2, 0xB, 0x9],
    [0x7, 0xE, 0x3, 0x9, 0x8, 0xA, 0xF, 0x0, 0x5, 0x2, 0x6, 0xC, 0xB, 0x1, 0xD, 0x4]
]

# Permutation matrix for ShiftRows
shift_rows_matrix = [
    [0, 1, 2, 3],
    [1, 2, 3, 0],
    [2, 3, 0, 1],
    [3, 0, 1, 2]
]

# Permutation matrix for mix_columns
mix_columns_matrix = [
    [0x2, 0x3, 0x1, 0x1],
    [0x1, 0x2, 0x3, 0x1],
    [0x1, 0x1, 0x2, 0x3],
    [0x3, 0x1, 0x1, 0x2]
]

# Permutation matrix for inv_mix_columns
inv_mix_columns_matrix = [
    [0xE, 0xB, 0xD, 0x9],
    [0x9, 0xE, 0xB, 0xD],
    [0xD, 0x9, 0xE, 0xB],
    [0xB, 0xD, 0x9, 0xE]
]

# Round key for AddRoundKey
round_key = [0x4, 0x5, 0x6, 0x7]

# SubBytes transformation
def sub_bytes(state):
    for i in range(4):
        for j in range(4):
            state[i][j] = s_box[state[i][j] >> 4][state[i][j] & 0x0F]
    return state

# Inverse SubBytes transformation
def sub_bytes_inv(state):
    for i in range(4):
        for j in range(4):
            byte_value = state[i][j]
            row = -1
            column = -1
            for r in range(2):
                for c in range(16):
                    if s_box[r][c] == byte_value:
                        row = r
                        column = c
                        break
                if row != -1:
                    break
            state[i][j] = (row << 4) | column
    return state

# ShiftRows transformation
def shift_rows(state_matrix, shift_matrix, inv):
    shifted_state = [[0] * 4 for _ in range(4)]
    for row in range(4):
        for col in range(4):
            if (inv):
                new_col = (col - shift_matrix[row][row]) % 4
            else:
                new_col = (col + shift_matrix[row][row]) % 4
            shifted_state[row][new_col] = state_matrix[row][col]
    return shifted_state

# MixColumns transformation
def mix_columns(state_matrix, mix_matrix):
    mixed_state = [[0] * 4 for _ in range(4)]
    for col in range(4):
        for row in range(4):
            mixed_state[row][col] = gf_add_mul(state_matrix, mix_matrix, row, col)
    return mixed_state

# Galois field addition and multiplication
def gf_add_mul(state_matrix, mix_matrix, row, col):
    p = 0
    for i in range(4):
        p ^= gf_mul(state_matrix[i][col], mix_matrix[row][i])
    return p

# Galois field multiplication
def gf_mul(a, b):
    p = 0
    for _ in range(4):
        if b & 1:
            p ^= a
        hi_bit_set = a & 0x8
        a <<= 1
        if hi_bit_set:
            a ^= 0x3
        b >>= 1
    return p & 0xF

# AddRoundKey transformation
def add_round_key(state, round_key):
    for i in range(4):
        for j in range(4):
            state[i][j] ^= round_key[j]
    return state

def print_state(state):
    for row in state:
        print([hex(x) for x in row])
        
def main():
    input_state = [
        [0x1, 0x2, 0x3, 0x4],
        [0x5, 0x6, 0x7, 0x8],
        [0x9, 0xA, 0xB, 0xC],
        [0xD, 0xE, 0xF, 0x0]
    ]

    print("Input State:")
    for row in input_state:
        print([hex(x) for x in row])

    sub_bytes_output = sub_bytes(input_state)
    print("\nAfter SubBytes:")
    for row in sub_bytes_output:
        print([hex(x) for x in row])

    shifted_state = shift_rows(sub_bytes_output, shift_rows_matrix, False)
    print("\nAfter ShiftRows:")
    for row in shifted_state:
        print([hex(x) for x in row])

    mix_columns_output = mix_columns(shifted_state, mix_columns_matrix)
    print("\nAfter MixColumns:")
    for row in mix_columns_output:
        print([hex(x) for x in row])

    add_round_key_output = add_round_key(mix_columns_output, round_key)
    print("\nAfter AddRoundKey:")
    for row in add_round_key_output:
        print([hex(x) for x in row])

    inverse_add_round_key_output = add_round_key(add_round_key_output, round_key)
    print("\nAfter Inverse AddRoundKey:")
    for row in inverse_add_round_key_output:
        print([hex(x) for x in row])

    inverse_inv_mix_matrix_output = mix_columns(inverse_add_round_key_output, inv_mix_columns_matrix)
    print("\nAfter Inverse MixColumns:")
    for row in inverse_inv_mix_matrix_output:
        print([hex(x) for x in row])

    inv_shifted_state_output = shift_rows(inverse_inv_mix_matrix_output, shift_rows_matrix, True)
    print("\nAfter Inverse ShiftRows:")
    for row in inv_shifted_state_output:
        print([hex(x) for x in row])

    inv_sub_bytes_output = sub_bytes_inv(inv_shifted_state_output)
    print("\nAfter Inverse SubBytes:")
    for row in inv_sub_bytes_output:
        print([hex(x) for x in row])

if __name__ == "__main__":
    main()