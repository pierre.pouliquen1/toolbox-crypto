from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes

def pad(message):
    block_size = AES.block_size
    padding_size = block_size - len(message) % block_size
    padding = bytes([padding_size] * padding_size)
    return message + padding

def unpad(message):
    padding_size = message[-1]
    return message[:-padding_size]

def encrypt(key, plaintext):
    cipher = AES.new(key, AES.MODE_ECB)
    padded_plaintext = pad(plaintext)
    ciphertext = cipher.encrypt(padded_plaintext)
    return ciphertext

def decrypt(key, ciphertext):
    cipher = AES.new(key, AES.MODE_ECB)
    padded_plaintext = cipher.decrypt(ciphertext)
    plaintext = unpad(padded_plaintext)
    return plaintext

def main():
    key = get_random_bytes(16)  # 16 bytes = 128 bits key
    plaintext = b"Hello, this is a simple AES encryption example."
    
    ciphertext = encrypt(key, plaintext)
    decrypted_text = decrypt(key, ciphertext)
    
    print("Original Plaintext:", plaintext)
    print("Ciphertext:", ciphertext)
    print("Decrypted Plaintext:", decrypted_text.decode("utf-8"))

if __name__ == "__main__":
    main()
