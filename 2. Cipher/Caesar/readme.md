# Caesar Cipher Encryption and Decryption

This Python script provides functionality for encrypting and decrypting text using the Caesar cipher.

## Script Explanation

- `caesar_encrypt(text, shift)`: Encrypts the input text using the Caesar cipher with a specified shift value.
- `caesar_decrypt(text, shift)`: Decrypts the input text encrypted using the Caesar cipher with a specified shift value.
- The `main()` function prompts the user to choose between encryption and decryption. It then prompts for the input text and performs the selected operation.

## Usage

1. Run the script.
2. Enter 'E' to encrypt or 'D' to decrypt when prompted.
3. If encrypting:
   - Enter the plaintext when prompted.
4. If decrypting:
   - Enter the ciphertext when prompted.
5. The script will perform the chosen operation and display the result.

## Note

- The Caesar cipher is a substitution cipher where each letter in the plaintext is shifted a certain number of places down or up the alphabet.
- The script assumes that the input text contains only alphabetic characters. Any non-alphabetic characters will remain unchanged in both encryption and decryption.