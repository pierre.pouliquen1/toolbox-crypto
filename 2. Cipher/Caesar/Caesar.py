def caesar_encrypt(text, shift):
    encrypted_text = ""
    for char in text:
        if char.isalpha():
            if char.isupper():
                encrypted_text += chr((ord(char) - 65 + shift) % 26 + 65)
            else:
                encrypted_text += chr((ord(char) - 97 + shift) % 26 + 97)
        else:
            encrypted_text += char
    return encrypted_text

def caesar_decrypt(text, shift):
    decrypted_text = ""
    for char in text:
        if char.isalpha():
            if char.isupper():
                decrypted_text += chr((ord(char) - 65 - shift) % 26 + 65)
            else:
                decrypted_text += chr((ord(char) - 97 - shift) % 26 + 97)
        else:
            decrypted_text += char
    return decrypted_text

def main():
    choice = input("Do you want to encrypt or decrypt? (e/d): ")
    if choice.lower() == 'e':
        plaintext = input("Enter the text to encrypt: ")
        shift = 3
        encrypted = caesar_encrypt(plaintext, shift)
        print("Encrypted text:", encrypted)
    elif choice.lower() == 'd':
        ciphertext = input("Enter the text to decrypt: ")
        shift = 3
        decrypted = caesar_decrypt(ciphertext, shift)
        print("Decrypted text:", decrypted)
    else:
        print("Invalid choice.")

if __name__ == "__main__":
    main()
