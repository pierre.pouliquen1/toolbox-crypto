# ElGamal Encryption Example

This repository contains a Python code example that demonstrates the ElGamal encryption scheme. ElGamal is a public-key cryptosystem that allows secure communication by encrypting messages using a recipient's public key and enabling them to decrypt it with their private key.

## Prerequisites

- Python 3.x

## How to Use

1. Clone this repository to your local machine
2. Run the Python script:
   ```
   python elgamal_encryption.py
   ```
5. Follow the on-screen prompts to input the required values.

## Steps of ElGamal Encryption

1. Input the prime number (p) and primitive root (g).
2. Input your private key (a).
3. Calculate your public key (A) using the provided formula.
4. Input the recipient's public key (B).
5. Input the plaintext message (m) you want to encrypt.
6. Generate a random value (k).
7. Calculate the shared secret (s) using the recipient's public key.
8. Calculate the ciphertext components c1 and c2.
9. Decrypt the message using the recipient's private key to retrieve the original plaintext.

## Notes

- Make sure to input prime numbers, primitive roots, private keys, public keys, and other required values during the execution of the script.
- This code example provides a basic demonstration and may require additional optimizations for real-world applications.
- **Never** use this code for actual security purposes without proper review and consideration of security best practices.
