def mod_pow(base, exponent, modulus):
    result = 1
    while exponent > 0:
        if exponent % 2 == 1:
            result = (result * base) % modulus
        exponent = exponent // 2
        base = (base * base) % modulus
    return result

def main():
    # Step 1: Input prime number (p) and its primitive root (g)
    p = int(input("Enter a prime number (p): "))
    g = int(input("Enter a primitive root (g): "))

    # Step 2: Input private key (a)
    a = int(input("Enter your private key (a): "))

    # Step 3: Calculate public key (A)
    A = mod_pow(g, a, p)
    print(f"Your public key (A) is: {A}")

    # Step 4: Input recipient's public key (B)
    B = int(input("Enter recipient's public key (B): "))

    # Step 5: Input plaintext message (m)
    m = int(input("Enter the plaintext message (m): "))

    # Step 6: Generate random value (k)
    k = int(input("Enter a random value (k): "))

    # Step 7: Calculate the shared secret (s)
    s = mod_pow(B, a, p)
    
    # Step 8: Calculate the ciphertext components
    c1 = mod_pow(g, k, p)
    c2 = (m * s ** k) % p

    print("Ciphertext components:")
    print(f"c1: {c1}")
    print(f"c2: {c2}")

    # Step 9: Decrypt the message using recipient's private key
    decrypted_message = (c2 * mod_pow(c1, p - 1 - B, p)) % p
    print(f"Decrypted message: {decrypted_message}")

if __name__ == "__main__":
    main()
