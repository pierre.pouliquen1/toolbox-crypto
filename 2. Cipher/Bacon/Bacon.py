def baconian_cipher(text, mode):
    baconian_alphabet = {
        'A': 'aaaaa', 'B': 'aaaab', 'C': 'aaaba', 'D': 'aaabb', 'E': 'aabaa',
        'F': 'aabab', 'G': 'aabba', 'H': 'aabbb', 'I': 'abaaa', 'J': 'abaab',
        'K': 'ababa', 'L': 'ababb', 'M': 'abbaa', 'N': 'abbab', 'O': 'abbba',
        'P': 'abbbb', 'Q': 'baaaa', 'R': 'baaab', 'S': 'baaba', 'T': 'baabb',
        'U': 'babaa', 'V': 'babab', 'W': 'babba', 'X': 'babbb', 'Y': 'bbaaa',
        'Z': 'bbaab'
    }

    result = ''
    if mode == 'encrypt':
        for char in text.upper():
            if char in baconian_alphabet:
                result += baconian_alphabet[char]
            else:
                result += char # keep non-alphabetic characters unchanged
    elif mode == 'decrypt':
        for i in range(0, len(text), 5):
            group = text[i:i+5]
            for char, code in baconian_alphabet.items():
                if group == code:
                    result += char
                    break
            else:
                result += group # keep non-cipher characters unchanged

    return result


def main():
    mode = input("Choose mode - 'encrypt' or 'decrypt': ").lower()
    if mode not in ['encrypt', 'decrypt']:
        print("Invalid mode. Please choose 'encrypt' or 'decrypt'.")
        return

    message = input("Enter the message: ")

    result = baconian_cipher(message, mode)
    print(f"{mode.capitalize()}ed message: {result}")


if __name__ == "__main__":
    main()
