# Baconian Cipher Encryption and Decryption

This Python script implements the Baconian Cipher for both encryption and decryption of text messages.

## Script Explanation

- `baconian_cipher(text, mode)`: Defines the function to perform encryption or decryption using the Baconian Cipher. It utilizes a dictionary `baconian_alphabet` mapping each uppercase letter to its corresponding Baconian code (5-bit binary representation). For encryption, it converts each letter of the input text to its Baconian code. For decryption, it groups the ciphertext into blocks of 5 characters and finds the corresponding letter for each block.
- The `main()` function prompts the user to choose between encryption and decryption. It then prompts for the input message and displays the encrypted or decrypted message accordingly.

## Usage

1. Run the script.
2. Choose whether you want to encrypt or decrypt a message by entering 'encrypt' or 'decrypt' when prompted.
3. Enter the message to be encrypted or decrypted when prompted.
4. The script will display the encrypted or decrypted message accordingly.

## Note

- The Baconian Cipher is a substitution cipher where each letter of the plaintext is replaced with a group of 5 characters representing its Baconian code. In this implementation, uppercase letters are encrypted to Baconian codes and vice versa for decryption. Any non-alphabetic characters are kept unchanged.
- The script assumes that the input message contains only uppercase letters. Any other characters will not be encrypted or decrypted.