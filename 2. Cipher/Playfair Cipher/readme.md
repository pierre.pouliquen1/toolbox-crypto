# Playfair Cipher Encryption

This Python script performs encryption using the Playfair Cipher. The Playfair Cipher is a manual symmetric encryption technique that uses a 5x5 grid of letters, called the Playfair Matrix, to encrypt pairs of letters in the plaintext.

## Functions

### `generate_playfair_matrix(key)`

Generates the Playfair Matrix using the given key. The key is used to fill the matrix, and the remaining letters of the alphabet are appended.

### `find_positions(matrix, char)`

Finds the row and column positions of a character `char` in the Playfair Matrix `matrix`.

### `playfair_encrypt(matrix, plaintext)`

Encrypts the plaintext using the Playfair Cipher and the provided Playfair Matrix `matrix`.

### `print_matrix(matrix)`

Prints the Playfair Matrix.

### `main()`

The main function where the user can input the key and plaintext. It generates the Playfair Matrix, encrypts the plaintext using the Playfair Cipher, and displays the encrypted text.

## Usage

1. Run the script.
2. Enter the key for the Playfair Cipher. The key should be a string of alphabetic characters.
3. Enter the plaintext message to encrypt.
4. The script will generate the Playfair Matrix, encrypt the plaintext using the Playfair Cipher, and display the encrypted text.

## Example

```
Enter the key: MONARCHY
Enter the plaintext: HELLO WORLD
```

```
Playfair Matrix:
M O N A R
C H Y B D
E F G I K
L P Q S T
U V W X Z

Plaintext: HELLO WORLD
Encrypted text: DOGGLACQDMRO
```

Feel free to use this script to encrypt messages using the Playfair Cipher!