import random
import string

def generate_playfair_matrix(key):
    key = key.replace("W", "V")
    key = key.upper()
    key = "".join(dict.fromkeys(key))  # Remove duplicates while maintaining order
    alphabet = "ABCDEFGHIKLMNOPQRSTUVWXYZ"
    
    for char in key:
        alphabet = alphabet.replace(char, "")

    key += alphabet

    matrix = [key[i:i+5] for i in range(0, len(key), 5)]
    return matrix

def find_positions(matrix, char):
    for i, row in enumerate(matrix):
        for j, val in enumerate(row):
            if val == char:
                return i, j

def playfair_encrypt(matrix, plaintext):
    plaintext = plaintext.replace("J", "I")
    plaintext = plaintext.upper()
    plaintext = plaintext.replace(" ", "")
    if len(plaintext) % 2 != 0:
        plaintext += "X"

    encrypted_text = ""
    for i in range(0, len(plaintext), 2):
        char1 = plaintext[i]
        char2 = plaintext[i+1]
        row1, col1 = find_positions(matrix, char1)
        row2, col2 = find_positions(matrix, char2)

        if row1 == row2:
            encrypted_text += matrix[row1][(col1 + 1) % 5]
            encrypted_text += matrix[row2][(col2 + 1) % 5]
        elif col1 == col2:
            encrypted_text += matrix[(row1 + 1) % 5][col1]
            encrypted_text += matrix[(row2 + 1) % 5][col2]
        else:
            encrypted_text += matrix[row1][col2]
            encrypted_text += matrix[row2][col1]

    return encrypted_text

def find_positions(matrix, char):
    for i, row in enumerate(matrix):
        if char in row:
            return i, row.index(char)

def print_matrix(matrix):
    for row in matrix:
        print(" ".join(row))

def main():
    key = input("Enter the key: ")
    matrix = generate_playfair_matrix(key)

    print("Playfair Matrix:")
    print_matrix(matrix)

    plaintext = input("Enter the plaintext: ")
    encrypted_text = playfair_encrypt(matrix, plaintext)
    
    print("Plaintext:", plaintext)
    print("Encrypted text:", encrypted_text)

if __name__ == "__main__":
    main()
