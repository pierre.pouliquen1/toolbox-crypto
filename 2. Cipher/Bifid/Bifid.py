import math
import string
from itertools import chain, zip_longest
from typing import Dict, Iterable, Iterator, Tuple, TypeVar

T = TypeVar("T")

def group(it: Iterable[T], n: int) -> Iterator[Tuple[T, ...]]:
    return zip_longest(*[iter(it)] * n)

Square = Tuple[Tuple[str, ...], ...]

def display_square(square: Square) -> None:
    for row in square:
        print(" ".join(row))

def polybius_square(alphabet: str) -> Square:
    return tuple(group(alphabet, math.ceil(math.sqrt(len(alphabet)))))

def polybius_map(square: Square) -> Dict[str, Tuple[int, int]]:
    return {square[i][j]: (i + 1, j + 1) for i in range(len(square)) for j in range(len(square))}

def encrypt(message: str, square: Square) -> str:
    _map = polybius_map(square)
    return "".join(square[x - 1][y - 1] for x, y in group(chain.from_iterable(zip(*(_map[c] for c in message if c in _map))), 2))

def decrypt(message: str, square: Square) -> str:
    _map = polybius_map(square)
    return "".join(square[x - 1][y - 1] for x, y in zip(*group(chain.from_iterable((_map[c] for c in message if c in _map)), len(message))))

def normalize(message: str) -> str:
    return message.upper().replace("J", "I")

def main() -> None:
    choice = input("Enter 'E' to encrypt or 'D' to decrypt: ").upper()
    if choice not in {'E', 'D'}:
        print("Invalid choice. Please enter 'E' for encrypt or 'D' for decrypt.")
        return

    message = input("Enter the message: ").upper()
    square = polybius_square("".join(c for c in string.ascii_uppercase if c != "J"))
    print("\nPolybius Square:")
    display_square(square)

    if choice == 'E':
        result = encrypt(normalize(message), square)
        print(f"\nEncrypted message: {result}")

    elif choice == 'D':
        result = decrypt(message, square)
        print(f"\nDecrypted message: {result}")

if __name__ == "__main__":
    main()