# Polybius Square Encryption and Decryption

This Python script implements the Polybius Square cipher for both encryption and decryption of text messages.

## Script Explanation

- `group(it: Iterable[T], n: int) -> Iterator[Tuple[T, ...]]`: Defines a function to group elements of an iterable into tuples of size `n`.
- `display_square(square: Square) -> None`: Defines a function to display the Polybius Square in a readable format.
- `polybius_square(alphabet: str) -> Square`: Generates the Polybius Square based on the given alphabet, ensuring that 'J' is excluded and converted to 'I'.
- `polybius_map(square: Square) -> Dict[str, Tuple[int, int]]`: Generates a mapping from characters to their coordinates in the Polybius Square.
- `encrypt(message: str, square: Square) -> str`: Encrypts a message using the Polybius Square cipher.
- `decrypt(message: str, square: Square) -> str`: Decrypts a message using the Polybius Square cipher.
- `normalize(message: str) -> str`: Normalizes the message by converting it to uppercase and replacing 'J' with 'I'.
- The `main()` function prompts the user to choose between encryption and decryption. It then prompts for the input message and displays the Polybius Square. Finally, it encrypts or decrypts the message accordingly.

## Usage

1. Run the script.
2. Enter 'E' to encrypt or 'D' to decrypt when prompted.
3. If encrypting:
   - Enter the message when prompted.
4. If decrypting:
   - Enter the ciphertext when prompted.
5. The script will display the Polybius Square and the encrypted or decrypted message accordingly.

## Note

- The Polybius Square cipher replaces each letter in the message with its coordinates in the square.
- The script assumes that the input message contains only uppercase letters. Any other characters will not be encrypted or decrypted.