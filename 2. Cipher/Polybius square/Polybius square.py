def create_polybius_square():
    alphabet = "ABCDEFGHIKLMNOPQRSTUVWXYZ"
    square = [[' ' for _ in range(5)] for _ in range(5)]
    chars = iter(alphabet + "0123456789")
    
    for i in range(5):
        for j in range(5):
            square[i][j] = next(chars)

    return square

def display_polybius_square(square):
    print("Polybius Square:")
    for row in square:
        print(" ".join(row))
    print()

def polybius_encrypt(message, square):
    message = message.upper()
    encrypted_message = ""

    for char in message:
        if char == 'J':
            char = 'I'

        for i in range(5):
            for j in range(5):
                if square[i][j] == char:
                    encrypted_message += str(i + 1) + str(j + 1)

    return encrypted_message

def polybius_decrypt(ciphertext, square):
    decrypted_message = ""

    for i in range(0, len(ciphertext), 2):
        row = int(ciphertext[i]) - 1
        col = int(ciphertext[i + 1]) - 1
        decrypted_message += square[row][col]

    return decrypted_message

if __name__ == "__main__":
    polybius_square = create_polybius_square()
    display_polybius_square(polybius_square)

    choice = input("Enter 'E' to encrypt or 'D' to decrypt: ").upper()

    if choice == 'E':
        plaintext = input("Enter the message to encrypt: ")
        ciphertext = polybius_encrypt(plaintext, polybius_square)
        print(f"Encrypted message: {ciphertext}")
    elif choice == 'D':
        ciphertext = input("Enter the message to decrypt: ")
        plaintext = polybius_decrypt(ciphertext, polybius_square)
        print(f"Decrypted message: {plaintext}")
    else:
        print("Invalid choice. Please enter 'E' or 'D'.")
