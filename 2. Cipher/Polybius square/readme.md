# Polybius Square Encryption and Decryption

This Python script performs encryption and decryption using the Polybius Square cipher. The Polybius Square is a substitution cipher where each letter of the plaintext is replaced by a pair of numerical coordinates representing its position in a grid.

## Functions

### `create_polybius_square()`

Generates the Polybius Square grid.

### `display_polybius_square(square)`

Prints the Polybius Square grid.

### `polybius_encrypt(message, square)`

Encrypts the plaintext message using the Polybius Square cipher and the provided grid.

### `polybius_decrypt(ciphertext, square)`

Decrypts the ciphertext message using the Polybius Square cipher and the provided grid.

## Usage

1. Run the script.
2. The Polybius Square grid will be generated and displayed.
3. Enter 'E' to encrypt or 'D' to decrypt.
4. If encrypting:
   - Enter the plaintext message.
   - The encrypted ciphertext will be displayed.
5. If decrypting:
   - Enter the ciphertext message.
   - The decrypted plaintext will be displayed.

## Example

```
Polybius Square:
A B C D E
F G H I K
L M N O P
Q R S T U
V W X Y Z

Enter 'E' to encrypt or 'D' to decrypt: E
Enter the message to encrypt: HELLO
Encrypted message: 3243411134

Enter 'E' to encrypt or 'D' to decrypt: D
Enter the message to decrypt: 3243411134
Decrypted message: HELLO
```

Feel free to use this script to encrypt and decrypt messages using the Polybius Square cipher!