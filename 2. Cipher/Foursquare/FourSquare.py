import re

table = [['A', 'B', 'C', 'D', 'E'],
         ['F', 'G', 'H', 'I', 'J'],
         ['K', 'L', 'M', 'N', 'O'],
         ['P', 'R', 'S', 'T', 'U'],
         ['V', 'W', 'X', 'Y', 'Z']]

def generate_table(key=''):
    alphabet = 'ABCDEFGHIJKLMNOPRSTUVWXYZ'
    table = [[0] * 5 for row in range(5)]
    key = re.sub(r'[\W]', '', key).upper()

    for row in range(5):
        for col in range(5):
            if len(key):
                table[row][col] = key[0]
                alphabet = alphabet.replace(key[0], '')
                key = key.replace(key[0], '')
            else:
                table[row][col] = alphabet[0]
                alphabet = alphabet[1:]
    return table

def print_square(table):
    for row in table:
        print(" ".join(row))

def encrypt(keys, words):
    ciphertext = ''
    words = re.sub(r'[\W]', '', words).upper().replace('Q', '')
    R, L = generate_table(keys[0]), generate_table(keys[1])

    print("\nSquare R:")
    print_square(R)
    print("\nSquare L:")
    print_square(L)

    for i in range(0, len(words), 2):
        digraphs = words[i:i+2]
        a = position(table, digraphs[0])
        b = position(table, digraphs[1])
        ciphertext += R[a[0]][b[1]] + L[b[0]][a[1]]

    return ciphertext

def decrypt(keys, words):
    plaintext = ''
    words = re.sub(r'[\W]', '', words).upper().replace('Q', '')
    R, L = generate_table(keys[0]), generate_table(keys[1])

    print("\nSquare R:")
    print_square(R)
    print("\nSquare L:")
    print_square(L)

    for i in range(0, len(words), 2):
        digraphs = words[i:i+2]
        a = position(R, digraphs[0])
        b = position(L, digraphs[1])
        plaintext += table[a[0]][b[1]] + table[b[0]][a[1]]

    return plaintext.lower()

def position(table, ch):
    for row in range(5):
        for col in range(5):
            if table[row][col] == ch:
                return (row, col)
    return (None, None)

def main():
    user_choice = input("Choose 'E' to encrypt or 'D' to decrypt: ").upper()
    key = input("Enter key: ").upper()

    if user_choice == 'E':
        plaintext = input("Enter the text to encrypt: ").upper()
        cipher_text = encrypt(key, plaintext)
        print("\nThe encrypted text is:", cipher_text)

    elif user_choice == 'D':
        cipher_text = input("Enter the text to decrypt: ").upper()
        recovered_text = decrypt(key, cipher_text)
        print("\nThe decrypted text is:", recovered_text)

    else:
        print("Invalid choice. Please enter 'E' to encrypt or 'D' to decrypt.")

if __name__ == "__main__":
    main()