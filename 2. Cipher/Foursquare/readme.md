# Polybius Square Cipher

This Python script provides functionalities to encrypt and decrypt messages using the Polybius Square cipher. The Polybius Square cipher is a substitution cipher that replaces each pair of letters in the plaintext with another pair of letters based on their positions in a 5x5 grid.

## Functions

### `generate_table(key='')`

Generates the Polybius Square table based on the provided key. If no key is provided, a default table is generated.

### `print_square(table)`

Prints the Polybius Square table.

### `encrypt(keys, words)`

Encrypts the given message using the Polybius Square cipher with the provided keys.

### `decrypt(keys, words)`

Decrypts the given ciphertext using the Polybius Square cipher with the provided keys.

### `position(table, ch)`

Finds the position of a character in the Polybius Square table.

### `main()`

The main function where the user can choose to encrypt or decrypt a message, input the necessary keys and message, and get the result.

## Usage

1. Run the script.
2. Choose whether to encrypt or decrypt.
3. Enter the encryption/decryption key.
4. Enter the message to encrypt/decrypt.
5. The script will perform the chosen operation and display the result.

## Example

```
Choose 'E' to encrypt or 'D' to decrypt: E
Enter key: EXAMPLE
Enter the text to encrypt: HELLO
```

```
The encrypted text is: XBEJNHRQ
```

```
Choose 'E' to encrypt or 'D' to decrypt: D
Enter key: EXAMPLE
Enter the text to decrypt: XBEJNHRQ
```

```
The decrypted text is: HELLO
```

Feel free to encrypt and decrypt messages using the Polybius Square cipher with this script!