import random

import math

 

def rotate_grid(grid):

    # Rotate the grid 90 degrees clockwise

    return list(zip(*grid[::-1]))

 

def print_grid(cipher,grid=0):

    n = len(cipher)

    ligneborder = "+" + "---+"*n

    for i in range(n):

        print(ligneborder)

        ligne="| "

        for j in range(n):

            if(grid ==0 or grid[i][j]==1):

                ligne+=str(cipher[i][j])+ " | "

            else:

                ligne+= '#' + " | "

        print(ligne)

    print(ligneborder+"\n")

 

def decrypt_grid(cipher, grid):

    message = ''

    n = len(cipher)

    for _ in range(4):

        print("Step:"+ str(_+1))

        print_grid(cipher, grid)

        for i in range(n):

            for j in range(n):

                if grid[i][j] == 1:

                    message += cipher[i][j]

       

        grid = rotate_grid(grid)

   

    return message

 

def create_grid(n):

    grid = [[0]*n for __ in range (n)]

    return grid

 

def full_filled(grid):

    for sublist in grid:

        for element in sublist:

            if element != 1:

                return True

    return False

 

def find_sym(n, i):

    if n % 2 == 0: # Si n est pair

        return (n - 1) - i

    else:

        if i == (n // 2):

            return i

        elif i < (n // 2):

            return (n - 1) - i

        else:

            return n - i -1

 

def create_holed_grid(n):

    tmp_grid = create_grid(n)

    n = len(tmp_grid)

    holes = []

    if n % 2 != 0:

        tmp_grid[n // 2][n // 2]=1

    while(full_filled(tmp_grid)):

        half = n // 2

        i = random.randint(0, n-1)

        j = random.randint(0, n-1)

        if (tmp_grid[i][j] !=1):

            holes.append([i,j])

            tmp_grid[i][j]=1

            for z in range(4):

                sym_i = find_sym(n,i)

                tmp_grid[j][sym_i]=1

                i = j

                j = sym_i

    grid = create_grid(n)

    for i in holes:

        grid[i[0]][i[1]] = 1

    return grid

 

def create_cypher_grid(holed_grid,message,n):

    cipher = create_grid(n)

    x = 0

    copy_holed_grid = holed_grid

    for _ in range(4):

        for i in range(n):

            for j in range(n):

                if(copy_holed_grid[i][j]==1):

                    if(x < len(message)):

                        cipher[i][j] = message[x]

                        x += 1

                    else:

                        cipher[i][j] = random.choice('abcdefghijklmnopqrstuvwxyz')

        copy_holed_grid = rotate_grid(copy_holed_grid)

    return cipher




# Example usage:

#cipher_test = [['H', 'L', 'W', 'R'], ['E', 'O', 'L', 'L'], ['L', 'C', 'O', 'D'], ['L', 'M', 'Y', 'O']]

#grid_test = [[0, 1, 0, 1], [1, 0, 1, 0], [0, 1, 0, 1], [1, 0, 1, 0]]

#grid2 = create_holed_grid(4)

 

#decrypted_message = decrypt_grid(cipher_test, grid_test)

#print("Decrypted Message:", decrypted_message)

while True:

    choice = input("Choose your program: \n 1. Decrypt  \n 2. Encrypt  \n →")

    if int(choice) == 1:

        cipher_string = input("Enter your string:")

        n = int(math.sqrt(len(cipher_string)))

        if (n**2 == len(cipher_string)):

            cipher = create_grid(n)

            pos_cyph = 0

            for i in range(n):

                for j in range(n):

                    cipher[i][j]= cipher_string[pos_cyph]

                    pos_cyph+=1

        else:

            print("La string doit avoir une longeur qui soit le carré d'un entier")

        grid_string = input("Enter your string:")

        if(len(grid_string) == len(cipher_string)):

            grid = create_grid(n)

            pos_grid = 0

            for i in range(n):

                for j in range(n):

                    grid[i][j]= int(grid_string[pos_grid])

                    pos_grid+=1

        else:

            print("Pas la bonne taille de grille")

        decrypted_message = decrypt_grid(cipher, grid)

        print("Decrypted Message:", decrypted_message)

    if int(choice) == 2:

        x = input("Enter your message:")

        n = 0

        while n*n < len(x):

            n+=1

        if (n*n == len(x) and n % 2 ==1):

            n+=1

        holed_grid = create_holed_grid(n)

        print_grid(holed_grid)

        print_grid(create_cypher_grid(holed_grid,x+".",n))
