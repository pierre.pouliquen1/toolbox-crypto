# Encryption and Decryption with Chao Cipher

This Python script allows you to encrypt and decrypt messages using the Chao cipher. The Chao cipher is a polygraphic substitution cipher that involves rotating and permuting elements in a grid.

## Functions

### `rotate_grid(grid)`

Rotates the grid 90 degrees clockwise.

### `print_grid(cipher, grid=0)`

Prints the grid with the cipher text.

### `decrypt_grid(cipher, grid)`

Decrypts the cipher text using the provided grid.

### `create_grid(n)`

Creates an empty grid of size `n x n`.

### `full_filled(grid)`

Checks if all elements in the grid are filled.

### `find_sym(n, i)`

Finds the symmetric index in the grid.

### `create_holed_grid(n)`

Creates a grid with holes (1s) randomly distributed.

### `create_cypher_grid(holed_grid, message, n)`

Creates a cipher grid using the holed grid and the message.

## Usage

1. Run the script.
2. Choose whether to decrypt or encrypt.
   - For decryption, enter the cipher text and the grid.
   - For encryption, enter the message to encrypt.
3. The script will perform the chosen operation and display the result.

## Example

Here's an example of using the script:

```
Choose your program:
1. Decrypt
2. Encrypt
→ 2
Enter your message: HELLO
+---+---+---+---+
| # | H | # | L |
+---+---+---+---+
| W | # | R | # |
+---+---+---+---+
| E | # | # | # |
+---+---+---+---+
| # | # | # | # |
+---+---+---+---+

+---+---+---+---+
| H | W | E | # |
+---+---+---+---+
| L | # | # | # |
+---+---+---+---+
| # | R | # | # |
+---+---+---+---+
| # | # | # | # |
+---+---+---+---+
```

## Note

- Ensure that the grid size is compatible with the message length.

Feel free to encrypt and decrypt messages using the Chao cipher with this script!