# IDEA encryption algorithm implementation

def multiply(a, b):
    if a == 0:
        a = 0x10001
    if b == 0:
        b = 0x10001
    return (a * b) % 0x10001

def add(a, b):
    return (a + b) % 0x10000

def sub(a, b):
    return (a - b + 0x10000) % 0x10000

def xor(a, b):
    return a ^ b

def encrypt_block(block, key):
    # Split block into 4 4-bit parts
    w1, w2, w3, w4 = block

    # Split key into 8 16-bit subkeys
    keys = [key[i:i+2] for i in range(0, len(key), 2)]

    # Round 1
    r1 = multiply(int(w1, 16), int(keys[0], 16))
    r2 = add(int(w2, 16), int(keys[1], 16))
    r3 = add(int(w3, 16), int(keys[2], 16))
    r4 = multiply(int(w4, 16), int(keys[3], 16))

    # Round 2
    r1 = xor(r1, int(keys[4], 16))
    r2 = sub(r2, int(keys[5], 16))
    r3 = sub(r3, int(keys[6], 16))
    r4 = xor(r4, int(keys[7], 16))

    # Round 3
    r1 = multiply(r1, int(keys[8], 16))
    r2 = add(r2, int(keys[9], 16))
    r3 = add(r3, int(keys[10], 16))
    r4 = multiply(r4, int(keys[11], 16))

    # Round 4
    r1 = xor(r1, int(keys[12], 16))
    r2 = sub(r2, int(keys[13], 16))
    r3 = sub(r3, int(keys[14], 16))
    r4 = xor(r4, int(keys[15], 16))

    encrypted_block = [format(r1, '04x'), format(r2, '04x'), format(r3, '04x'), format(r4, '04x')]
    return encrypted_block

def inverse(a):
    if a == 0:
        return 0
    return pow(a, 0xFFFF, 0x10001)

def decrypt_block(block, key):
    keys = [key[i:i+2] for i in range(0, len(key), 2)]

    # Calculate inverse keys for decryption
    inv_keys = []
    for i in range(0, len(keys), 2):
        inv_keys.append(format(inverse(int(keys[i], 16)), '02x'))
        inv_keys.append(format(sub(0x10000, int(keys[i+1], 16)), '02x'))
    
    # Round 1
    r1 = multiply(int(block[0], 16), int(inv_keys[0], 16))
    r2 = sub(int(block[1], 16), int(inv_keys[1], 16))
    r3 = sub(int(block[2], 16), int(inv_keys[2], 16))
    r4 = multiply(int(block[3], 16), int(inv_keys[3], 16))
    
    # Round 2
    r1 = xor(r1, int(inv_keys[4], 16))
    r2 = add(r2, int(inv_keys[5], 16))
    r3 = add(r3, int(inv_keys[6], 16))
    r4 = xor(r4, int(inv_keys[7], 16))
    
    # Round 3
    r1 = multiply(r1, int(inv_keys[8], 16))
    r2 = sub(r2, int(inv_keys[9], 16))
    r3 = sub(r3, int(inv_keys[10], 16))
    r4 = multiply(r4, int(inv_keys[11], 16))
    
    # Round 4
    r1 = xor(r1, int(inv_keys[12], 16))
    r2 = add(r2, int(inv_keys[13], 16))
    r3 = add(r3, int(inv_keys[14], 16))
    r4 = xor(r4, int(inv_keys[15], 16))
    
    decrypted_block = [format(r1, '04x'), format(r2, '04x'), format(r3, '04x'), format(r4, '04x')]
    return decrypted_block

def main():
    print("IDEA Encryption and Decryption Example (4-bit blocks)")
    key = input("Enter a 128-bit hexadecimal key: ")
    plaintext = input("Enter a 16-bit hexadecimal plaintext block: ")

    # Check and adjust input lengths
    key = key[:32]
    plaintext = plaintext[:4]

    encrypted_block = encrypt_block(plaintext, key)
    decrypted_block = decrypt_block(encrypted_block, key)
    
    print("\nEncryption:")
    print(f"Plaintext: {plaintext}")
    print(f"Key: {key}")
    print(f"Encrypted Block: {' '.join(encrypted_block)}")

    print("\nDecryption:")
    print(f"Encrypted Block: {' '.join(encrypted_block)}")
    print(f"Decrypted Block: {' '.join(decrypted_block)}")

if __name__ == "__main__":
    main()