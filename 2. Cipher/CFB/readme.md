Sure, here's the README in English explaining the provided code using both the non-library and library-based approaches:

## Cipher Feedback (CFB) Encryption Example

### Non-Library Approach

This Python code demonstrates a basic implementation of the Cipher Feedback (CFB) encryption mode. In CFB mode, the output of the encryption process is fed back into the cipher for subsequent rounds. This example employs a simple XOR-based CFB encryption with an 8-bit block size.

#### Usage

1. Run the script.
2. Input a key (0-255) for encryption.
3. Enter the plaintext you want to encrypt.
4. The script will display the encrypted data and then decrypt it to show the original plaintext.

### Library-Based Approach

This Python code showcases CFB encryption and decryption using the `cryptography` library. The `cryptography` library provides high-level APIs for secure cryptography operations.

#### Prerequisites

To run this code, you need to have the `cryptography` library installed. You can install it using the following command:

```bash
pip install cryptography
```

#### Usage

1. Run the script.
2. A random 128-bit key and initialization vector (IV) are generated.
3. Input the plaintext you want to encrypt.
4. The script will display the ciphertext and then decrypt it to show the original plaintext.

Please note that the library-based approach is more robust and secure for actual usage due to its utilization of well-established cryptographic primitives and best practices.

Remember that these examples are meant for educational purposes. In a real-world scenario, you should follow established cryptographic guidelines and practices to ensure the security of your data.

---

Feel free to use these explanations as your README content. Just make sure to adjust the formatting and any additional details as needed.