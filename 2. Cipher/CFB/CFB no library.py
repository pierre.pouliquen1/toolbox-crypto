def encrypt_block(key, block):
    encrypted_block = key ^ block
    return encrypted_block

def cfb_encrypt(key, plaintext):
    ciphertext = []
    iv = 0x55  # Initialization Vector
    for char in plaintext:
        encrypted_iv = encrypt_block(key, iv)
        ciphertext_char = encrypted_iv ^ char
        ciphertext.append(ciphertext_char)
        iv = ciphertext_char
    return bytes(ciphertext)

def decrypt_block(key, encrypted_block):
    decrypted_block = key ^ encrypted_block
    return decrypted_block

def cfb_decrypt(key, ciphertext):
    plaintext = []
    iv = 0x55  # Initialization Vector
    for char in ciphertext:
        encrypted_iv = encrypt_block(key, iv)
        decrypted_char = char ^ encrypted_iv
        plaintext.append(decrypted_char)
        iv = char
    return bytes(plaintext)

def main():
    key = int(input("Enter the key (0-255): "))
    plaintext = input("Enter the plaintext: ")

    plaintext_bytes = plaintext.encode('utf-8')
    encrypted_data = cfb_encrypt(key, plaintext_bytes)
    decrypted_data = cfb_decrypt(key, encrypted_data)

    print("\nEncrypted Data:", encrypted_data.hex())
    print("Decrypted Data:", decrypted_data.decode('utf-8'))

if __name__ == "__main__":
    main()
