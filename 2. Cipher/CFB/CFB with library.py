from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.ciphers import CipherContext
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import padding
import os

def cfb_encrypt(key, iv, plaintext):
    cipher = Cipher(algorithms.AES(key), modes.CFB(iv), backend=default_backend())
    encryptor = cipher.encryptor()
    ciphertext = encryptor.update(plaintext) + encryptor.finalize()
    return ciphertext

def cfb_decrypt(key, iv, ciphertext):
    cipher = Cipher(algorithms.AES(key), modes.CFB(iv), backend=default_backend())
    decryptor = cipher.decryptor()
    plaintext = decryptor.update(ciphertext) + decryptor.finalize()
    return plaintext

def main():
    key = os.urandom(16)  # Generate a random 128-bit key
    iv = os.urandom(16)   # Generate a random initialization vector

    print("Enter the plaintext:")
    plaintext = input().encode('utf-8')

    ciphertext = cfb_encrypt(key, iv, plaintext)
    print("Ciphertext:", ciphertext.hex())

    decrypted_plaintext = cfb_decrypt(key, iv, ciphertext)
    print("Decrypted Plaintext:", decrypted_plaintext.decode('utf-8'))

if __name__ == "__main__":
    main()
