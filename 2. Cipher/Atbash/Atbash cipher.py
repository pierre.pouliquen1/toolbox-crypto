lookup_table = {
    'A': 'Z', 'B': 'Y', 'C': 'X', 'D': 'W',
    'E': 'V', 'F': 'U', 'G': 'T', 'H': 'S',
    'I': 'R', 'J': 'Q', 'K': 'P', 'L': 'O',
    'M': 'N', 'N': 'M', 'O': 'L', 'P': 'K',
    'Q': 'J', 'R': 'I', 'S': 'H', 'T': 'G',
    'U': 'F', 'V': 'E', 'W': 'D', 'X': 'C',
    'Y': 'B', 'Z': 'A'
}

def atbash(message):
    cipher = ""
    for letter in message:
        if letter != ' ':
            cipher += lookup_table[letter.upper()]
        else:
            cipher += ' '
    return cipher

def main():
    choice = input("Do you want to encrypt or decrypt? (e/d): ")
    plaintext = input("Enter the text : ").upper()
    ciphered_text = atbash(plaintext)
    if choice.lower() == 'e':
        print("Encrypted text:", ciphered_text)
    elif choice.lower() == 'd':
        print("Decrypted text:", ciphered_text)
    else:
        print("Invalid choice.")

if __name__ == "__main__":
    main()