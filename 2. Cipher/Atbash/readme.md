# Atbash Cipher Encryption and Decryption

This Python script implements the Atbash Cipher for both encryption and decryption of text messages.

## Script Explanation

- `lookup_table`: Defines a dictionary that maps each letter of the alphabet to its corresponding letter in the Atbash Cipher. For example, 'A' is mapped to 'Z', 'B' to 'Y', and so on.
- `atbash(message)`: Defines the function to encrypt or decrypt a message using the Atbash Cipher. It iterates through each character in the message, converts it to uppercase, and looks up its corresponding letter in the `lookup_table`. Spaces are preserved in the output.
- The `main()` function prompts the user to choose between encryption and decryption. It then prompts for the input text and displays the encrypted or decrypted text accordingly.

## Usage

1. Run the script.
2. Choose whether you want to encrypt or decrypt a message by entering 'e' for encryption or 'd' for decryption.
3. Enter the text when prompted.
4. The script will display the encrypted or decrypted message accordingly.

## Note

- The Atbash Cipher is a substitution cipher where each letter in the plaintext is replaced with its corresponding letter in the reversed alphabet. For example, 'A' becomes 'Z', 'B' becomes 'Y', and so on.
- The script assumes that the input message contains only uppercase letters and spaces. Any other characters will not be encrypted or decrypted.