# Rail Fence Cipher

This Python script implements the Rail Fence Cipher for encryption and decryption. The Rail Fence Cipher is a transposition cipher that writes plaintext in a zigzag pattern across multiple "rails" (rows) of an imaginary fence.

## Cipher Rules
- For encryption:
  - The plaintext is written diagonally across a specified number of "rails" (rows).
  - Each rail corresponds to a row in an imaginary grid.
  - The ciphertext is formed by reading the characters row by row.
- For decryption:
  - The ciphertext is written diagonally across the rails.
  - The plaintext is formed by reading the characters diagonally.

## Functions

### `printRailGrid(rail)`
Prints the rail grid, showing the arrangement of characters in the Rail Fence Cipher.

### `constructRailGrid(length, key)`
Constructs the rail grid based on the length of the plaintext and the number of rails (key).

### `applyRailFence(text, key, encrypt=True)`
Applies the Rail Fence Cipher to either encrypt or decrypt the input text, depending on the value of the `encrypt` parameter.

### `encryptRailFence(text, key)`
Encrypts the input text using the Rail Fence Cipher with the specified key.

### `decryptRailFence(cipher, key)`
Decrypts the input ciphertext using the Rail Fence Cipher with the specified key.

### `main()`
The main function where the user can choose to encrypt or decrypt a message.

## Usage

1. Run the script.
2. Choose 'E' to encrypt or 'D' to decrypt.
3. Enter the key rail (the number of rails).
4. Enter the text to encrypt or decrypt.
5. The encrypted or decrypted text will be displayed accordingly.

## Example

```
Choose 'E' to encrypt or 'D' to decrypt: E
Enter key rail: 3
Enter the text to encrypt: Hello World
The encrypted text is: HooWrdell l

Choose 'E' to encrypt or 'D' to decrypt: D
Enter key rail: 3
Enter the text to decrypt: HooWrdell l
The decrypted text is: Hello World
```

Feel free to use this script to encrypt and decrypt messages using the Rail Fence Cipher!