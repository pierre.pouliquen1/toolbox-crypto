def printRailGrid(rail):
    for row in rail:
        print(' '.join(cell if cell != '\n' else ' ' for cell in row))
    print()


def constructRailGrid(length, key):
    rail = [['\n' for _ in range(length)] for _ in range(key)]
    return rail

def applyRailFence(text, key, encrypt=True):
    rail = constructRailGrid(len(text), key)
    dir_down = False
    row, col = 0, 0

    for i in range(len(text)):
        if row == 0 or row == key - 1:
            dir_down = not dir_down
        rail[row][col] = text[i]
        col += 1
        if dir_down:
            row += 1
        else:
            row -= 1

    result = []

    for i in range(key):
        for j in range(len(text)):
            if rail[i][j] != '\n':
                result.append(rail[i][j])

    printRailGrid(rail) # Ajout pour afficher la RailGrid

    return ''.join(result) if encrypt else rail


def encryptRailFence(text, key):
    return applyRailFence(text, key, encrypt=True)

def decryptRailFence(cipher, key):
    rail = applyRailFence(cipher, key, encrypt=False)
    result = []
    row, col = 0, 0

    for i in range(len(cipher)):
        if row == 0:
            dir_down = True
        if row == key - 1:
            dir_down = False

        if rail[row][col] != '\n':
            result.append(rail[row][col])
            col += 1

        if dir_down:
            row += 1
        else:
            row -= 1

    return ''.join(result)

def main():
    user_choice = input("Choose 'E' to encrypt or 'D' to decrypt: ").upper()
    key = int(input("Enter key rail: "))

    if user_choice == 'E':
        plaintext = input("Enter the text to encrypt: ").upper()
        cipher_text = encryptRailFence(plaintext, key)
        print("\nThe encrypted text is:", cipher_text)
    elif user_choice == 'D':
        cipher_text = input("Enter the text to decrypt: ").upper()
        recovered_text = decryptRailFence(cipher_text, key)
        print("\nThe decrypted text is:", recovered_text)
    else:
        print("Invalid choice. Please enter 'E' to encrypt or 'D' to decrypt.")

if __name__ == "__main__":
    main()
