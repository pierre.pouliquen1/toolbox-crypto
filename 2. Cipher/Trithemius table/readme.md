#Trithemius table

This Python script implements the Trithemius cipher, a polyalphabetic substitution cipher. The key idea behind the Trithemius cipher is to shift each letter of the plaintext by a variable amount based on a predetermined sequence or rule.

## `TrithemiusCipher` class

### Methods:

- `main()`: Main method to interact with the user, allowing encryption or decryption of messages.
- `encrypt(secret_message, shift)`: Encrypts the given secret message using the Trithemius cipher with the specified initial shift.
- `decrypt(secret_message, shift)`: Decrypts the given secret message using the Trithemius cipher with the specified initial shift.

## `Alphabet` class

This class contains the alphabet used for encryption and decryption.

## Usage

1. Run the script.
2. Choose whether to encrypt or decrypt.
3. Enter the message to encrypt/decrypt.
4. Enter the initial shift value (0 is default).
5. The script will print the encrypted/decrypted message.

## Example

```
Do you want to Encrypt or Decrypt?
1. Encrypt
2. Decrypt

1

Enter message to encrypt: 
Hello World

Enter initial shift (0 is default): 
3

Encrypted message: khoqwtlepsr
```

In this example, the plaintext "Hello World" is encrypted using the Trithemius cipher with an initial shift of 3, resulting in the ciphertext "khoqwtlepsr".

```
Do you want to Encrypt or Decrypt?
1. Encrypt
2. Decrypt

2

Enter message to decrypt: 
khoqwtlepsr

Enter initial shift (0 is default): 
3

Decrypted message: hello world
```

In this example, the ciphertext "khoqwtlepsr" is decrypted using the Trithemius cipher with an initial shift of 3, resulting in the plaintext "hello world".

Feel free to use this script to encrypt and decrypt messages using the Trithemius cipher!