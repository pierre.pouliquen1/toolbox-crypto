class TrithemiusCipher:
    @staticmethod
    def main():
        print("Do you want to Encrypt or Decrypt?\n1. Encrypt\n2. Decrypt\n")
        option = int(input())

        if option == 1:
            print("\nEnter message to encrypt: ")
            encrypt_message = input()

            print("\nEnter initial shift (0 is default): ")
            initial_shift_enc = int(input())

            print("\nEncrypted message:", TrithemiusCipher.encrypt(encrypt_message.lower(), initial_shift_enc))

        elif option == 2:
            print("\nEnter message to decrypt: ")
            decrypt_message = input()

            print("\nEnter initial shift (0 is default): ")
            initial_shift_dec = int(input())

            print("\nDecrypted message:", TrithemiusCipher.decrypt(decrypt_message.lower(), initial_shift_dec))

        else:
            print("Enter a valid value.")

    @staticmethod
    def encrypt(secret_message, shift):
        encrypted_message = []

        for char in secret_message:
            if char != ' ':
                encrypted_message.append(Alphabet.alphabet[(Alphabet.alphabet.index(char) + shift) % 26])
                shift += 1
            else:
                encrypted_message.append(char)

        return ''.join(encrypted_message)

    @staticmethod
    def decrypt(secret_message, shift):
        encrypted_message = []

        for char in secret_message:
            if char != ' ':
                encrypted_message.append(Alphabet.alphabet[(Alphabet.alphabet.index(char) - shift + 26) % 26])
                shift += 1
            else:
                encrypted_message.append(char)

        return ''.join(encrypted_message)


class Alphabet:
    alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
                'v', 'w', 'x', 'y', 'z']


# Run the program
TrithemiusCipher.main()
