# Affine Cipher Encryption and Decryption

This Python script implements the Affine Cipher algorithm for both encryption and decryption. The Affine Cipher is a type of monoalphabetic substitution cipher, where each letter in an alphabet is mapped to its numeric equivalent, encrypted using a simple mathematical function, and then converted back to a letter.

## Functions

### `affine_encrypt(text, shift)`
Encrypts the given text using the Affine Cipher algorithm with the specified shift.

### `affine_decrypt(text, shift)`
Decrypts the given text using the Affine Cipher algorithm with the specified shift.

### `main()`
The main function where the user can choose whether to encrypt or decrypt text.

## Usage

1. Run the script.
2. Choose whether to encrypt or decrypt text.
   - If encrypting:
     - Enter the text to encrypt.
     - Enter the shift value.
   - If decrypting:
     - Enter the text to decrypt.
     - Enter the shift value.
3. The encrypted or decrypted text will be printed.

## Example

```
Do you want to encrypt or decrypt? (e/d): e
Enter the text to encrypt: Hello, World!
Enter the shift: 3
Encrypted text: Khoor, Zruog!

Do you want to encrypt or decrypt? (e/d): d
Enter the text to decrypt: Khoor, Zruog!
Enter the shift: 3
Decrypted text: Hello, World!
```

Feel free to use this script to encrypt and decrypt text using the Affine Cipher algorithm!