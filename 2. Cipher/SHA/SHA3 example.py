# Operations functions to simulate SHA-3
def rotate_left(x, n):
    return ((x << n) | (x >> (16 - n))) & 0xFFFF

def xor(a, b):
    return a ^ b

def simulate_theta(a, b, c):
    return xor(xor(a, b), c)

def simulate_rho(a, n):
    return rotate_left(a, n)

def simulate_pi(a):
    return (a + 1) % 16

def simulate_chi(a, b, c):
    return xor(a, b), b, c

# Simulate one round of SHA-3
def simulate_sha3_round(state):
    new_state = [0] * 16

    # Simulate theta function
    for i in range(16):
        new_state[i] = simulate_theta(state[i], state[(i + 1) % 16], state[(i + 2) % 16])

    # Simulate rho rotations
    for i in range(16):
        new_state[i] = simulate_rho(new_state[i], i)

    # Simulate pi permutation
    temp_state = new_state.copy()
    for i in range(16):
        new_state[simulate_pi(i)] = temp_state[i]

    # Simulate chi function
    for i in range(0, 16, 3):
        new_state[i], new_state[i + 1], new_state[i + 2] = simulate_chi(new_state[i], new_state[i + 1], new_state[i + 2])

    return new_state

# Simulate SHA-3 with 16 bits
def simulate_sha3(message):
    # Initialize state
    state = [0x0000] * 16
    
    # Initial filling with message data (simplified)
    for i in range(len(message)):
        state[i % 16] ^= ord(message[i])

    # Simulate rounds
    for _ in range(10):  # 10 rounds for this example
        state = simulate_sha3_round(state)
    
    # Convert state to hexadecimal string
    hash_result = ''.join(format(x, '04x') for x in state)
    return hash_result

def main():
    # Message to hash
    message = "Hello, world!"

    # Call the SHA-3 simulation function
    hash_result = simulate_sha3(message)
    print("Message:", message)
    print("Hash:", hash_result)

if __name__ == "__main__":
    main()
