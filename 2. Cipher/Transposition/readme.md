# Transposition Cipher with Random Key

This Python script implements a transposition cipher with a randomly generated key. The transposition cipher works by rearranging the characters in the plaintext according to the key.

## Functions

### `generate_key()`
Generates a random key by shuffling the lowercase alphabet.

### `remove_punctuation_and_accents(text)`
Removes punctuation and replaces accented characters with their unaccented counterparts in the input text.

### `encrypt(message, key)`
Encrypts the message using the transposition cipher and the given key.

### `decrypt(ciphertext, key)`
Decrypts the ciphertext using the transposition cipher and the given key.

### `main()`
The main function where the user can choose whether to encrypt or decrypt text.

## Usage

1. Run the script.
2. Choose whether to encrypt or decrypt text.
   - If encrypting:
     - Enter the message to encrypt.
   - If decrypting:
     - Enter the ciphertext to decrypt.
     - Enter the transposition key used for encryption.
3. The encrypted or decrypted text will be printed.

## Example

```
Do you want to encrypt or decrypt? (e/d): e
Enter the message to encrypt: Hello World!
Transposition Key: u i f w s g e k q x l t p a r v j h m n o z b c d y
Encrypted Text: okkxsulxms

Do you want to encrypt or decrypt? (e/d): d
Enter the message to decrypt: okkxsulxms
Enter the transposition key: u i f w s g e k q x l t p a r v j h m n o z b c d y
Decrypted Text: helloworld
```

Feel free to use this script to encrypt and decrypt messages using the transposition cipher with a randomly generated key!