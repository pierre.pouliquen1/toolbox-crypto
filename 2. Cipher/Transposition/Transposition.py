import random
import string

def generate_key():
    alphabet = list(string.ascii_lowercase)
    key = alphabet.copy()
    random.shuffle(key)
    return key

def remove_punctuation_and_accents(text):
    text = text.lower()
    text = text.translate(str.maketrans('', '', string.punctuation))
    text = text.replace("é", "e").replace("è", "e").replace("à", "a")
    return text

def encrypt(message, key):
    message = remove_punctuation_and_accents(message)
    encrypted_message = []
    for char in message:
        if char.isalpha():
            encrypted_message.append(key[ord(char) - ord('a')])
    return ''.join(encrypted_message)

def decrypt(ciphertext, key):
    decrypted_message = []
    for char in ciphertext:
        if char.isalpha():
            decrypted_message.append(chr(key.index(char) + ord('a')))
    return ''.join(decrypted_message)

def main():
    choice = input("Do you want to encrypt or decrypt? (e/d): ").lower()
    if choice == 'e':
        plaintext = input("Enter the message to encrypt: ")
        key = generate_key()
        print("Transposition Key:", ' '.join(key))
        encrypted_text = encrypt(plaintext, key)
        print("Encrypted Text:", encrypted_text)
    elif choice == 'd':
        ciphertext = input("Enter the message to decrypt: ")
        key = input("Enter the transposition key: ").split()
        decrypted_text = decrypt(ciphertext, key)
        print("Decrypted Text:", decrypted_text)
    else:
        print("Invalid choice!")

if __name__ == "__main__":
    main()
