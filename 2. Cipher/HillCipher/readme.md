# Hill Cipher Encryption and Decryption

This Python script performs encryption and decryption using the Hill Cipher. The Hill Cipher is a polygraphic substitution cipher based on linear algebra. It encrypts and decrypts messages by multiplying a plaintext matrix with a key matrix (modulo 26) to produce a ciphertext matrix.

## Functions

### `encryption(a, b)`

Encrypts the given plaintext string `b` using the key matrix `a`.

### `letter_matrix(n)`

Converts the numerical matrix `n` back to a string of letters.

### `MatrixGenerator()`

Generates the key matrix and its inverse for encryption and decryption.

### `main()`

The main function where the user can input the message for encryption and decryption. It generates the key matrix and its inverse, encrypts and decrypts the message, and displays the results.

## Usage

1. Run the script.
2. Enter the message for encryption and decryption.
3. The script will generate the key matrix and its inverse, encrypt and decrypt the message using the Hill Cipher, and display the results.

## Example

```
Enter the message for encryption & decryption : HELLO WORLD
```

```
Matrix 
 [[1 1]
 [2 3]]
Inversed matrix 
 [[ 3 -1]
 [-2  1]]
The crypted text is:  JZSVQY BDZFGE
The decrypted text is:  HELLO WORLD
```

Feel free to use this script to encrypt and decrypt messages using the Hill Cipher!