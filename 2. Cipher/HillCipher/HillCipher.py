import numpy as np

def encryption(a, b):
    encrypted_matrix = [(np.matmul(a, [ord(b[i])-97, ord(b[i+1])-97]) % 26) for i in range(0, len(b), 2)]
    return letter_matrix(encrypted_matrix)

def letter_matrix(n):
    encrypted_string = ''
    for i in n:
        for j in i:
            encrypted_string += chr(j+97)
    return encrypted_string

def MatrixGenerator():
    matrix = np.array([[1,1],[2,3]])
    inv = np.linalg.inv(matrix)
    int_inv = inv.astype(int)
    return matrix, int_inv

def main():
    plaintext = input("Enter the message for encryption & decryption : ")
    word_list = plaintext.split(" ")

    encrypted = ''
    decrypted = ''
    matrix, int_inv = MatrixGenerator()

    for i in word_list:
        if len(i) % 2 != 0:
            i = i + 'a'
        encrypted += " " + encryption(matrix, i)
        decrypted += " " + encryption(int_inv, encryption(matrix, i))[0:len(i)]
    if(len(decrypted)%2==1):
        decrypted=decrypted[:-1]

    print("Matrix \n", matrix)
    print("Inversed matrix \n", int_inv)
    print("The crypted text is:", encrypted)
    print("The decrypted text is:", decrypted)

if __name__ == "__main__":
    main()
