# Rabin Cryptosystem

The Rabin Cryptosystem is a public-key cryptosystem based on the difficulty of integer factorization. It allows secure communication between parties using asymmetric encryption and decryption. This code provides a Python implementation of the Rabin Cryptosystem, allowing users to encrypt and decrypt messages.

## How It Works

The Rabin Cryptosystem involves generating a pair of primes (p and q) and their product (N). The message to be sent is converted into an integer, encrypted using the modulus operation, and decrypted using mathematical operations based on the properties of the Blum integers.

### Components

1. `gcd(a, b)`: A function to compute the greatest common divisor and extended gcd of two integers.

2. `blum_prime(bit_length)`: A function to generate Blum primes, which are prime numbers congruent to 3 modulo 4.

3. `generate_key(bit_length)`: A function to generate the public and private keys required for encryption and decryption.

4. `encrypt(m, N)`: A function to encrypt a message (integer) using the Rabin Cryptosystem.

5. `decrypt(c, p, q)`: A function to decrypt a ciphertext (integer) using the Rabin Cryptosystem.

6. `main()`: The main function that facilitates user interaction. Users can enter messages, which are then encrypted and decrypted using the Rabin Cryptosystem.

## Usage

1. Run the script.

2. Enter a message when prompted.

3. The script will display the encrypted message, and then decrypt it to verify the process.

## Dependencies

None. The code only uses Python's built-in libraries.

## Note

This implementation is for educational purposes and may not be suitable for production-level security. Cryptographic algorithms should be used carefully and securely to ensure proper protection of sensitive data.

Feel free to modify and enhance the code as needed for your understanding or use case.

