import random
import math

def gcd(a, b):
    s, old_s = 0, 1
    t, old_t = 1, 0
    r, old_r = b, a

    while r != 0:
        quotient = old_r // r
        old_r, r = r, old_r - quotient * r
        old_s, s = s, old_s - quotient * s
        old_t, t = t, old_t - quotient * t

    return old_r, old_s, old_t

def blum_prime(bit_length):
    while True:
        p = random.randint(2**(bit_length - 1), 2**bit_length - 1)
        if p % 4 == 3 and is_probable_prime(p):
            return p

def is_probable_prime(n, k=5):
    if n <= 1:
        return False
    if n <= 3:
        return True
    if n % 2 == 0:
        return False

    r, s = 0, n - 1
    while s % 2 == 0:
        r += 1
        s //= 2

    for _ in range(k):
        a = random.randint(2, n - 1)
        x = pow(a, s, n)
        if x == 1 or x == n - 1:
            continue
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True

def generate_key(bit_length):
    p = blum_prime(bit_length // 2)
    q = blum_prime(bit_length // 2)
    N = p * q
    return N, p, q

def encrypt(m, N):
    return pow(m, 2, N)

def decrypt(c, p, q):
    N = p * q
    p1 = pow(c, (p + 1) // 4, p)
    p2 = p - p1
    q1 = pow(c, (q + 1) // 4, q)
    q2 = q - q1

    _, y_p, y_q = gcd(p, q)

    d1 = (y_p * p * q1 + y_q * q * p1) % N
    d2 = (y_p * p * q2 + y_q * q * p1) % N
    d3 = (y_p * p * q1 + y_q * q * p2) % N
    d4 = (y_p * p * q2 + y_q * q * p2) % N

    return d1, d2, d3, d4

def main():
    bit_length = 512
    n, p, q = generate_key(bit_length)

    message = input("Enter your message: ")
    print("Message sent by the sender:", message)

    m = int.from_bytes(message.encode('latin-1'), byteorder='big')
    c = encrypt(m, n)

    print("Encrypted Message:", c)

    decrypted_messages = decrypt(c, p, q)
    final_message = None
    for b in decrypted_messages:
        dec = b.to_bytes(math.ceil(b.bit_length() / 8), byteorder='big').decode('latin-1').strip('\x00')
        if dec == message:
            final_message = dec

    print("Message received by the receiver:", final_message)

if __name__ == "__main__":
    main()
