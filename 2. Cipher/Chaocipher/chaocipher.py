def chao(in_text, mode, show_steps=False):
    l_alphabet = "HXUCZVAMDSLKPEFJRIGTWOBNYQ"
    r_alphabet = "PTLNBQDEOYSFAVZKGJRIHWXUMC"

    left = list(l_alphabet)
    right = list(r_alphabet)
    temp = [''] * 26

    out_text = [''] * len(in_text)

    for i in range(len(in_text)):
        if show_steps:
            print(" ".join(left), " ".join(right))

        if mode == "ENCRYPT":
            index = right.index(in_text[i])
            out_text[i] = left[index]
        else:
            index = left.index(in_text[i])
            out_text[i] = right[index]

        if i == len(in_text) - 1:
            break

        # permute left
        temp[:26 - index] = left[index:]
        temp[26 - index:] = left[:index]
        store = temp[1]
        temp[1:14] = temp[2:15]
        temp[13] = store
        left = temp.copy()

        # permute right
        temp[:26 - index] = right[index:]
        temp[26 - index:] = right[:index]
        store = temp[0]
        temp[0:25] = temp[1:26]
        temp[25] = store
        store = temp[2]
        temp[2:13] = temp[3:14]
        temp[13] = store
        right = temp.copy()

    return "".join(out_text)


def main():
    user_choice = input("Choose 'E' to encrypt or 'D' to decrypt: ").upper()

    if user_choice == 'E':
        plain_text = input("Enter the text to encrypt: ").upper()
        cipher_text = chao(plain_text, "ENCRYPT", True)
        print("\nThe encrypted text is:", cipher_text)

    elif user_choice == 'D':
        cipher_text = input("Enter the text to decrypt: ").upper()
        recovered_text = chao(cipher_text, "DECRYPT", False)
        print("\nThe decrypted text is:", recovered_text)

    else:
        print("Invalid choice. Please enter 'E' to encrypt or 'D' to decrypt.")

if __name__ == "__main__":
    main()



