# Chao Cipher Encryption and Decryption

This Python script implements the Chao cipher for both encryption and decryption of text messages.

## Chao Cipher

The Chao cipher is a polygraphic substitution cipher where each letter in the plaintext is substituted with another letter according to a specific rule. It involves permuting two halves of an alphabet.

## How to Use

1. Run the script.
2. Choose 'E' to encrypt or 'D' to decrypt when prompted.
3. If encrypting:
   - Enter the plaintext when prompted.
4. If decrypting:
   - Enter the ciphertext when prompted.
5. The script will perform the chosen operation and display the result.

## Example

Here's an example of using the script:

```
Choose 'E' to encrypt or 'D' to decrypt: E
Enter the text to encrypt: HELLO
HXUCZ VAMDS LKPEF JRIGT WOBNY Q
The encrypted text is: HXUCZVAMDSLKPEFJRIGTWOBNYQ
```

```
Choose 'E' to encrypt or 'D' to decrypt: D
Enter the text to decrypt: HXUCZVAMDSLKPEFJRIGTWOBNYQ
The decrypted text is: HELLO
```

## Note

- In encryption mode, the script displays intermediate steps by default. Set `show_steps` to `True` if you want to see these steps.

Feel free to encrypt and decrypt messages using the Chao cipher with this script!