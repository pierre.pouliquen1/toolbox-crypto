def vigenere(input_text, key, mode):
    input_text = input_text.upper()
    output = ""

    if mode == "ENCRYPT":
        res = []
        j = 0
        for i in range(len(input_text)):
            c = input_text[i]
            if not ('A' <= c <= 'Z'):
                continue
            res.append(chr((ord(c) + ord(key[j]) - 2 * ord('A')) % 26 + ord('A')))
            j = (j + 1) % len(key)
        output = ''.join(res)

    if mode == "DECRYPT":
        res = []
        j = 0
        for i in range(len(input_text)):
            c = input_text[i]
            if not ('A' <= c <= 'Z'):
                continue
            res.append(chr((ord(c) - ord(key[j]) + 26) % 26 + ord('A')))
            j = (j + 1) % len(key)
        output = ''.join(res)

    return output

def main():
    input_text = input("Enter the text: ")
    key = input("Enter the key: ")
    mode = input("Enter mode (ENCRYPT or DECRYPT): ")

    if mode not in ["ENCRYPT", "DECRYPT"]:
        print("Invalid mode. Please enter either ENCRYPT or DECRYPT.")
        return

    result = vigenere(input_text, key, mode)
    print("Result:", result)

if __name__ == "__main__":
    main()
