# Vigenère Cipher Implementation

This repository contains a Python implementation of the Vigenère cipher. The Vigenère cipher is a method of encrypting alphabetic text by using a simple form of polyalphabetic substitution. A polyalphabetic cipher uses multiple substitution alphabets to encrypt the plaintext. The Vigenère cipher is a form of a polyalphabetic substitution.

## Features

- Encrypt plaintext using the Vigenère cipher with a given key.
- Decrypt ciphertext using the Vigenère cipher with a given key.

## How to Use

1. Clone the repository:

    ```bash
    git clone https://github.com/example/vigenere-cipher.git
    ```

2. Navigate to the project directory:

    ```bash
    cd vigenere-cipher
    ```

3. Run the Python script:

    ```bash
    python vigenere_cipher.py
    ```

4. Follow the prompts to enter the input text, key, and choose encryption or decryption mode.

## Usage Example

```python
from vigenere_cipher import vigenere

input_text = "HELLO"
key = "KEY"
mode = "ENCRYPT"

result = vigenere(input_text, key, mode)
print("Result:", result)
```
