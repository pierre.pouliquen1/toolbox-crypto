from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
from Crypto.Random import get_random_bytes

def encrypt(plaintext, key, iv):
    cipher = AES.new(key, AES.MODE_OFB, iv=iv)
    ciphertext = cipher.encrypt(pad(plaintext, AES.block_size))
    return ciphertext

def decrypt(ciphertext, key, iv):
    cipher = AES.new(key, AES.MODE_OFB, iv=iv)
    decrypted_data = unpad(cipher.decrypt(ciphertext), AES.block_size)
    return decrypted_data

def main():
    key = get_random_bytes(16)
    iv = get_random_bytes(16)

    print("Key : ", key)
    print("iv : ", iv)
    plaintext = input("Enter the plaintext: ").encode('utf-8')

    ciphertext = encrypt(plaintext, key, iv)
    print("Ciphertext:", ciphertext.hex())

    decrypted_data = decrypt(ciphertext, key, iv)
    print("Decrypted text:", decrypted_data.decode('utf-8'))

if __name__ == "__main__":
    main()
