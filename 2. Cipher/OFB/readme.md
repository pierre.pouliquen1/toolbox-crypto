# OFB Encryption and Decryption Scripts

These Python scripts demonstrate the concept of Output Feedback (OFB) mode encryption and decryption. The OFB mode is a block cipher mode that transforms a block cipher into a stream cipher.


## Setup

1. Clone or download the repository to your local machine.

2. Open a terminal/command prompt and navigate to the directory containing the scripts.

3. Install the required libraries using the following command:

    ```
    pip install pycryptodome
    ```

## Usage

### Script 1: Using Crypto Library

1. Open a terminal/command prompt.

2. Navigate to the directory containing the scripts.

3. Run the script using the command:

    ```
    python ofb_crypto.py
    ```

4. Enter the plaintext message when prompted.

5. The script will generate a random encryption key and initialization vector (IV), encrypt the input using OFB mode, display the ciphertext, and then decrypt the ciphertext to show the original plaintext.

### Script 2: Without Using Library

1. Open a terminal/command prompt.

2. Navigate to the directory containing the scripts.

3. Run the script using the command:

    ```
    python ofb_custom.py
    ```

4. Enter the plaintext message when prompted.

5. The script will use custom implementations to perform OFB encryption and decryption. It will display the ciphertext and then decrypt it to show the original plaintext.

## Security Note

While these scripts provide a basic demonstration of the OFB encryption and decryption process, it's crucial to use well-established cryptographic libraries and best practices when implementing encryption in real-world applications. The scripts are not optimized for security or performance and are intended for educational purposes only.