def xor_bytes(a, b):
    return bytes(x ^ y for x, y in zip(a, b))

def encrypt_block(plaintext_block, key, iv):
    cipher = xor_bytes(plaintext_block, iv)
    encrypted_block = xor_bytes(cipher, key)
    return encrypted_block

def decrypt_block(ciphertext_block, key, iv):
    decrypted_block = xor_bytes(ciphertext_block, key)
    decrypted_block = xor_bytes(decrypted_block, iv)
    return decrypted_block

def encrypt(plaintext, key, iv):
    ciphertext = b''
    for i in range(0, len(plaintext), 16):
        plaintext_block = plaintext[i:i + 16]
        encrypted_block = encrypt_block(plaintext_block, key, iv)
        ciphertext += encrypted_block
        iv = encrypted_block
    return ciphertext

def decrypt(ciphertext, key, iv):
    plaintext = b''
    for i in range(0, len(ciphertext), 16):
        ciphertext_block = ciphertext[i:i + 16]
        decrypted_block = decrypt_block(ciphertext_block, key, iv)
        plaintext += decrypted_block
        iv = ciphertext_block
    return plaintext

def main():
    key = b'\x01\x23\x45\x67\x89\xAB\xCD\xEF\xFE\xDC\xBA\x98\x76\x54\x32\x10'
    iv = b'\x10\x32\x54\x76\x98\xBA\xDC\xFE\xEF\xCD\xAB\x89\x67\x45\x23\x01'

    print("Key : ", key)
    print("iv : ", iv)
    plaintext = input("Enter the plaintext: ").encode('utf-8')

    ciphertext = encrypt(plaintext, key, iv)
    print("Ciphertext:", ciphertext.hex())

    decrypted_data = decrypt(ciphertext, key, iv)
    print("Decrypted text:", decrypted_data.decode('utf-8'))

if __name__ == "__main__":
    main()
