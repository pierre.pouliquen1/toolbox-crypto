# Initial permutation table
initial_permutation = [1, 3, 2, 0]

# Initial permutation function
def initial_permute(data, permutation_table):
    return [data[i] for i in permutation_table]

# Encryption
def encrypt(plain_text, key):
    # Initial permutation
    permuted_text = initial_permute(plain_text, initial_permutation)
    
    # XOR with key
    xor_result = [str(int(permuted_text[i]) ^ int(key[i])) for i in range(4)]
    
    print("After initial permutation and XOR with key:")
    print("Intermediate result:", ''.join(xor_result))
    
    # Split into two parts
    left_half = xor_result[:2]
    right_half = xor_result[2:]
    
    print("Left half:", ''.join(left_half))
    print("Right half:", ''.join(right_half))
    
    # Swap halves
    swapped_text = right_half + left_half
    
    print("After swapping halves:", ''.join(swapped_text))
    
    # Final permutation
    encrypted_text = initial_permute(swapped_text, initial_permutation)
    
    print("Encrypted result:", ''.join(encrypted_text))
    
    return ''.join(encrypted_text)

# Decryption
def decrypt(ciphertext, key):
    # Initial permutation
    permuted_text = initial_permute(ciphertext, initial_permutation)
    
    # Swap halves
    left_half = permuted_text[:2]
    right_half = permuted_text[2:]
    
    # Swap halves back
    swapped_text = right_half + left_half
    
    # XOR with key
    xor_result = [str(int(swapped_text[i]) ^ int(key[i])) for i in range(4)]
    
    # Final permutation
    decrypted_text = initial_permute(xor_result, initial_permutation)
    
    return ''.join(decrypted_text)

def main():
    plain_text = input("Enter 4-bit plaintext: ")
    key = input("Enter 4-bit key: ")

    encrypted = encrypt(plain_text, key)
    print("Encrypted Text:", encrypted)

    decrypted = decrypt(encrypted, key)
    print("Decrypted Text:", decrypted)

if __name__ == "__main__":
    main()
