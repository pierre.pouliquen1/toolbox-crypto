# Simplified DES Algorithm Example

This is a simple Python implementation of the Data Encryption Standard (DES) algorithm. The example focuses on a 4-bit block size for educational purposes.

## Usage

1. Clone the repository:

2. Run the `DES_example.py` script:
   ```
   python des_example.py
   ```

3. Follow the prompts to enter a 4-bit plaintext and a 4-bit key.

4. The script will output the intermediate steps of encryption and decryption, as well as the final results.

## Note

- This example provides a simplified view of the DES algorithm for educational purposes and is not suitable for actual encryption purposes.
- The example uses a 4-bit block size, which is much smaller than the typical 64-bit block size used in the actual DES algorithm.
- Use this code to understand the basic concepts of DES, but keep in mind that the actual algorithm is much more complex and secure.
