import socket

def encrypt_message(message, pub_key):
    e, n = pub_key
    encrypted_message = [(ord(char) ** e) % n for char in message]
    return ','.join(map(str, encrypted_message))

def alice_main():
    alice_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    alice_socket.connect(('127.0.0.1', 12345))

    pub_key_str = alice_socket.recv(1024).decode()
    e, n = map(int, pub_key_str[1:-1].split(', '))

    print("Received Bob's Public Key (e, n):", (e, n))

    message = input("Enter the message to send to Bob: ")

    encrypted_message = encrypt_message(message, (e, n))
    print("Encrypted Message:", encrypted_message)

    alice_socket.send(encrypted_message.encode())

    alice_socket.close()

if __name__ == "__main__":
    alice_main()
