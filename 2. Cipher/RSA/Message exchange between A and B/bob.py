import socket
import random

def generate_prime():
    primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
    return random.choice(primes)

def gcd(a, b):
    while b:
        a, b = b, a % b
    return a

def mod_inverse(e, phi):
    def extended_gcd(a, b):
        if b == 0:
            return a, 1, 0
        else:
            d, x, y = extended_gcd(b, a % b)
            return d, y, x - y * (a // b)
    
    d, x, y = extended_gcd(e, phi)
    return x % phi

def bob_main():
    p = generate_prime()
    q = generate_prime()
    n = p * q
    phi = (p - 1) * (q - 1)
    e = 3  # Public exponent
    d = mod_inverse(e, phi)  # Private exponent

    print("Bob's Public Key (e, n):", (e, n))
    print("Bob's Private Key (d, n):", (d, n))

    bob_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    bob_socket.bind(('127.0.0.1', 12345))
    bob_socket.listen(1)
    print("Bob's socket is listening...")

    alice_socket, address = bob_socket.accept()
    print("Alice connected from:", address)

    pub_key_str = str((e, n))
    alice_socket.send(pub_key_str.encode())

    encrypted_message = alice_socket.recv(1024).decode()

    decrypted_message = [chr((int(c) ** d) % n) for c in encrypted_message.split(',')]
    decrypted_message = ''.join(decrypted_message)
    print("Bob received and decrypted the message:", decrypted_message)

    alice_socket.close()
    bob_socket.close()

if __name__ == "__main__":
    bob_main()
