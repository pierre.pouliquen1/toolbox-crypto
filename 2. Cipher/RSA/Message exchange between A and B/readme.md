# Secure Communication with RSA Encryption

This project demonstrates a simple secure communication scenario using the RSA encryption algorithm. It includes two Python scripts: one for Bob (the recipient) and one for Alice (the sender). These scripts simulate the process of generating public and private keys, encrypting and decrypting messages, and exchanging data using sockets.

## Requirements

- Python 3.x

## Instructions

1. Clone this repository or download the files for Bob and Alice.

2. Open two separate terminal windows or command prompts.

3. In one terminal, run the Bob script:

    ```sh
    python bob.py
    ```

   This will start Bob's script, which will generate public and private keys, listen for incoming connections from Alice, and decrypt messages received from Alice.

4. In the other terminal, run the Alice script:

    ```sh
    python alice.py
    ```

   This will start Alice's script, which will connect to Bob, generate an encrypted message using Bob's public key, and send the encrypted message to Bob.

5. Follow the prompts in the Alice script to enter the message you want to send to Bob.

6. Observe the output in both terminals. Bob will display the decrypted message, and Alice will display the encrypted message she sent.

## Notes

- This project uses very small prime numbers for demonstration purposes only. In practice, larger prime numbers should be used for security.

- The scripts use basic socket communication for simplicity. In a real-world scenario, you might use more secure communication protocols.

- The code provided here is meant for educational purposes and should not be used in production for secure communication. Real-world implementations of RSA require careful consideration of security practices and optimizations.
