# RSA Encryption and Decryption in Python

This repository contains two Python scripts that demonstrate the RSA encryption and decryption algorithm. The first script uses prime numbers below 100 for demonstration purposes, while the second script provides a more realistic implementation of RSA.

## 1. RSA Implementation with Small Prime Numbers

### Introduction

This script showcases the RSA encryption and decryption process using small prime numbers. Note that this example is **not suitable for secure cryptographic purposes** and should only be used for educational purposes or simple demonstrations.

### How to Use

1. Open the `rsa_small_primes.py` script in a Python interpreter or an IDE.
2. Run the script.
3. The script generates small prime numbers, computes encryption and decryption keys, and encrypts and decrypts a sample message.

## 2. RSA Implementation with Realistic Prime Numbers

### Introduction

This script provides a proper implementation of the RSA encryption and decryption algorithm using randomly generated large prime numbers. It demonstrates the key generation, encryption, and decryption process of the RSA algorithm.

### How to Use

1. Open the `rsa_realistic.py` script in a Python interpreter or an IDE.
2. Run the script.
3. The script generates large prime numbers, computes encryption and decryption keys, and encrypts and decrypts a sample message.

### Important Note

In real-world scenarios, RSA encryption requires prime numbers with a significantly higher number of bits for security. The prime numbers used in this script are only suitable for educational purposes and should not be used in production systems.

## Disclaimer

These scripts are meant for educational and illustrative purposes only. They are not intended for use in security-critical applications. The security of RSA encryption depends on the use of large prime numbers, careful key management, and secure implementation practices.

