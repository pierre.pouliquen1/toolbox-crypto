import random

# Step 1: Generate two large prime numbers, p and q
def generate_prime(bits):
    while True:
        num = random.getrandbits(bits)
        if num > 1 and is_prime(num):
            return num

def is_prime(n, k=5):
    if n <= 1:
        return False
    if n <= 3:
        return True
    if n % 2 == 0 or n % 3 == 0:
        return False

    for _ in range(k):
        a = random.randint(2, n - 2)
        if pow(a, n - 1, n) != 1:
            return False
    return True

# Step 2: Compute n (modulus) and phi(n) (Euler's totient function)
def compute_modulus_phi(p, q):
    n = p * q
    phi_n = (p - 1) * (q - 1)
    return n, phi_n

# Step 3: Choose an encryption exponent (public key)
def choose_public_exponent(phi_n):
    e = 65537  # Commonly used public exponent
    while gcd(e, phi_n) != 1:
        e += 2
    return e

def gcd(a, b):
    while b:
        a, b = b, a % b
    return a

# Step 4: Compute the decryption exponent (private key)
def compute_private_exponent(e, phi_n):
    d = mod_inverse(e, phi_n)
    return d

def mod_inverse(a, m):
    g, x, y = extended_gcd(a, m)
    if g != 1:
        raise ValueError("Modular inverse does not exist")
    return x % m

def extended_gcd(a, b):
    if a == 0:
        return b, 0, 1
    g, x, y = extended_gcd(b % a, a)
    return g, y - (b // a) * x, x

# Step 5: Encryption and Decryption functions
def encrypt(message, e, n):
    encrypted_message = [pow(ord(char), e, n) for char in message]
    return encrypted_message

def decrypt(encrypted_message, d, n):
    decrypted_message = ''.join([chr(pow(char, d, n)) for char in encrypted_message])
    return decrypted_message

# Step 6: Main RSA process
def main():
    bits = 1024  # Number of bits in prime numbers (adjust as needed)
    p = generate_prime(bits)
    q = generate_prime(bits)
    
    n, phi_n = compute_modulus_phi(p, q)
    e = choose_public_exponent(phi_n)
    d = compute_private_exponent(e, phi_n)


    print("p : ", p)
    print("q : ", q)
    print("e : ", e)
    print("d : ", d)
    message = input("Enter your message:")
    print("Original message:", message)

    encrypted_message = encrypt(message, e, n)
    print("Encrypted message:", encrypted_message)

    decrypted_message = decrypt(encrypted_message, d, n)
    print("Decrypted message:", decrypted_message)

if __name__ == "__main__":
    main()
