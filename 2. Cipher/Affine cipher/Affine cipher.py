# Key values of a and b
#a = 17
#b = 20

def encrypt_message(plaintext,a,b):
    # Cipher Text initially empty
    cipher = ""
    for char in plaintext:
        # Avoid space to be encrypted
        if char != ' ':
            # applying encryption formula ( a x + b ) mod m
            # {here x is ord(char) and m is 26} and added ord('A') to bring it in range of ASCII alphabet[ 65-90 | A-Z ]
            cipher += chr(((a * (ord(char) - ord('A')) + b) % 26) + ord('A'))
        else:
            # else simply append space character
            cipher += char
    return cipher

def decrypt_cipher(cipher,a,b):
    msg = ""
    a_inv = 0
    flag = 0

    # Find a^-1 (the multiplicative inverse of a in the group of integers modulo m.)
    for i in range(26):
        flag = (a * i) % 26
        # Check if (a*i)%26 == 1, then i will be the multiplicative inverse of a
        if flag == 1:
            a_inv = i

    for char in cipher:
        if char != ' ':
            # Applying decryption formula a^-1 ( x - b ) mod m
            # {here x is ord(char) and m is 26} and added ord('A') to bring it in range of ASCII alphabet[ 65-90 | A-Z ]
            msg += chr((a_inv * (ord(char) - ord('A') - b) % 26) + ord('A'))
        else:
            # else simply append space character
            msg += char

    return msg

def main():
    choice = input("Do you want to encrypt or decrypt? (e/d): ")
    a = int(input("Choose a by default 17 : ").strip() or "17")
    b = int(input("Choose a by default 20 : ").strip() or "20")
    if choice.lower() == 'e':
        plaintext = input("Enter the text to encrypt: ").upper()
        cipher_text = encrypt_message(plaintext,a,b)
        print("Encrypted text:", cipher_text)
    elif choice.lower() == 'd':
        ciphertext = input("Enter the text to decrypt: ").upper()
        decrypted = decrypt_cipher(ciphertext,a,b)
        print("Decrypted text:", decrypted)
    else:
        print("Invalid choice.")

if __name__ == "__main__":
    main()