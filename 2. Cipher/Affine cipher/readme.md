# Affine Cipher Encryption and Decryption

This Python script implements the Affine Cipher for encryption and decryption of text messages.

## Script Explanation

- `encrypt_message(plaintext, a, b)`: Defines the function to encrypt a plaintext message using the Affine Cipher. It iterates through each character in the plaintext, encrypts it using the formula \( E(x) = (ax + b) \mod 26 \), and returns the cipher text.
- `decrypt_cipher(cipher, a, b)`: Defines the function to decrypt a cipher text using the Affine Cipher. It calculates the multiplicative inverse \( a^{-1} \) of \( a \) modulo 26, then decrypts each character using the formula \( D(x) = a^{-1} (x - b) \mod 26 \), and returns the decrypted message.
- The `main()` function prompts the user to choose between encryption and decryption. If encryption is chosen, it prompts for the plaintext message and displays the encrypted text. If decryption is chosen, it prompts for the cipher text and displays the decrypted message.
- The default values of \( a \) and \( b \) are 17 and 20, respectively, but the user can choose different values if desired.

## Usage

1. Run the script.
2. Choose whether you want to encrypt or decrypt a message by entering 'e' for encryption or 'd' for decryption.
3. If encrypting, enter the plaintext message when prompted.
4. If decrypting, enter the cipher text when prompted.
5. The script will display the encrypted or decrypted message accordingly.

## Note

- The script assumes that the input message contains only uppercase letters and spaces. Any other characters will not be encrypted or decrypted.
- The Affine Cipher is a type of substitution cipher where each letter in the plaintext is mapped to another letter in the ciphertext based on a mathematical function. It is a special case of the more general Caesar Cipher with a fixed shift of 1.