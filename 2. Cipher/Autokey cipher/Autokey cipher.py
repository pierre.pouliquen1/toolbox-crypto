import re
alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def autokey_encryption(text, key):
    len_text = len(text)

    # Generating the keystream
    new_key = key + text
    new_key = new_key[:len(new_key) - len(key)]
    encrypt_text = ""

    # Applying encryption algorithm
    for x in range(len_text):
        first = alphabet.index(text[x])
        second = alphabet.index(new_key[x])
        total = (first + second) % 26
        encrypt_text += alphabet[total]

    return encrypt_text

def autokey_decryption(text, key):
    current_key = key
    decrypt_text = ""

    # Applying decryption algorithm
    for x in range(len(text)):
        get1 = alphabet.index(text[x])
        get2 = alphabet.index(current_key[x])
        total = (get1 - get2) % 26
        total = (total + 26) if total < 0 else total
        decrypt_text += alphabet[total]
        current_key += alphabet[total]

    return decrypt_text

def main():
    key = input(("Choose key by default N : ").strip() or "N").upper()
    # Using regular expression to check if key is a number
    if re.match(r"[-+]?[0-9]*\.?[0-9]+", key):
        key = alphabet[int(key) % 26]
    choice = input("Do you want to encrypt or decrypt? (e/d): ")
    print("Key : ", key)
    plaintext = input("Enter the text : ").upper()
    if choice.lower() == 'e':
        ciphered_text = autokey_encryption(plaintext, key)
        print("Encrypted text:", ciphered_text)
    elif choice.lower() == 'd':
        ciphered_text = autokey_decryption(plaintext, key)
        print("Decrypted text:", ciphered_text)
    else:
        print("Invalid choice.")

if __name__ == "__main__":
    main()
