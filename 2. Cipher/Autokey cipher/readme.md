# Autokey Cipher Encryption and Decryption

This Python script implements the Autokey Cipher for both encryption and decryption of text messages.

## Script Explanation

- `autokey_encryption(text, key)`: Defines the function to encrypt a plaintext message using the Autokey Cipher. It generates the keystream by appending the plaintext to the key, then applies the encryption algorithm to each character in the plaintext and returns the encrypted text.
- `autokey_decryption(text, key)`: Defines the function to decrypt a cipher text using the Autokey Cipher. It iteratively builds the key by appending the decrypted characters to the initial key and applies the decryption algorithm to each character in the cipher text, returning the decrypted message.
- The `main()` function prompts the user to choose between encryption and decryption, inputs the key, and the text to be encrypted or decrypted. It then displays the encrypted or decrypted message accordingly.

## Usage

1. Run the script.
2. Choose whether you want to encrypt or decrypt a message by entering 'e' for encryption or 'd' for decryption.
3. Enter the key when prompted. If the key is a number, it will be converted to its corresponding alphabet character (e.g., 0 corresponds to 'A', 1 corresponds to 'B', etc.).
4. Enter the text to be encrypted or decrypted when prompted.
5. The script will display the encrypted or decrypted message accordingly.

## Note

- The Autokey Cipher is a substitution cipher that uses a key to generate the keystream. It extends the key to match the length of the plaintext and then uses the keystream to encrypt or decrypt the message.
- The script assumes that the input message contains only uppercase letters. Any other characters will not be encrypted or decrypted.