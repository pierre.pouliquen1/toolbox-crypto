import random

def generate_random_key(length):
    key = []
    for _ in range(length):
        key.append(random.randint(0, 255))
    return key

def encrypt(message, key):
    encrypted_message = []
    for i in range(len(message)):
        encrypted_message.append(message[i] ^ key[i])  # XOR operation
    return encrypted_message

def decrypt(encrypted_message, key):
    decrypted_message = []
    for i in range(len(encrypted_message)):
        decrypted_message.append(encrypted_message[i] ^ key[i])  # XOR operation
    return decrypted_message

def main():
    message = input("Enter the message to encrypt: ")
    
    message_bytes = message.encode('utf-8')
    key = generate_random_key(len(message_bytes))

    encrypted_message = encrypt(message_bytes, key)
    decrypted_message = decrypt(encrypted_message, key)

    print("Original Message:", message)
    print("Key : ", key)
    print("Encrypted Message:", encrypted_message)
    print("Decrypted Message:", bytes(decrypted_message).decode('utf-8'))

if __name__ == "__main__":
    main()
