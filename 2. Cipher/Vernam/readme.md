# One-Time Pad Encryption

This Python script implements a basic one-time pad encryption and decryption system using the XOR operation.

## Functions

### `generate_random_key(length)`
Generates a random key of the specified length.

### `encrypt(message, key)`
Encrypts the message using the provided key using the XOR operation.

### `decrypt(encrypted_message, key)`
Decrypts the encrypted message using the provided key using the XOR operation.

### `main()`
The main function where the user can input the message to encrypt. The script then generates a random key of the same length as the message, encrypts the message using the key, and then decrypts it using the same key. The original message, the key, the encrypted message, and the decrypted message are printed.

## Usage

1. Run the script.
2. Enter the message to encrypt when prompted.
3. The script will generate a random key, encrypt the message, and then decrypt it using the same key.
4. The original message, the key, the encrypted message, and the decrypted message will be printed.

## Example

```
Enter the message to encrypt: Hello, world!
Original Message: Hello, world!
Key :  [158, 86, 100, 238, 73, 96, 222, 150, 77, 59, 155, 40, 194, 245, 184, 248, 228, 224, 105, 238, 220, 98, 105, 206, 3, 204, 93, 19, 108, 222, 75, 52, 94, 97, 62, 39, 97]
Encrypted Message: [226, 30, 24, 185, 3, 25, 93, 254, 79, 99, 218, 99, 102, 86, 68, 130, 184, 155, 62, 150, 18, 38, 52, 155, 191, 202, 19, 75, 31, 212, 137, 34, 12, 123, 47, 71, 59]
Decrypted Message: Hello, world!
```

In this example, the message "Hello, world!" is encrypted using a randomly generated key. The original message, the key, the encrypted message, and the decrypted message are printed. As you can see, the decrypted message matches the original message, indicating successful encryption and decryption.

Feel free to use this script to encrypt and decrypt messages using the one-time pad encryption method!