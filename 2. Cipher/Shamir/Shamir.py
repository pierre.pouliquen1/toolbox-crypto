import random

# Function to generate random coefficients to create a polynomial
def generate_coefficients(k, secret):
    coefficients = [secret]
    for _ in range(1, k):
        coefficients.append(random.randint(1, 100))  # Random choice of coefficients
    return coefficients

# Function to evaluate the polynomial at a point x
def evaluate_polynomial(coefficients, x):
    result = 0
    for i, coeff in enumerate(coefficients):
        result += coeff * (x ** i)
    return result

# Function to generate shares
def generate_shares(k, n, secret):
    coefficients = generate_coefficients(k, secret)
    shares = []
    for _ in range(n):
        x = random.randint(1, 1000)  # Generate a random x
        y = evaluate_polynomial(coefficients, x)  # Calculate y using the polynomial
        shares.append((x, y))  # Add the share to the list of shares
    return shares

# Function to reconstruct the secret from the shares
def reconstruct_secret(shares):
    secret = 0
    for i in range(len(shares)):
        xi, yi = shares[i]
        term = yi
        for j in range(len(shares)):
            if i != j:
                xj, _ = shares[j]
                term *= -xj / (xi - xj)
        secret += term
    return int(round(secret))

# Main function to create or reconstruct shares
def main():
    print("Choose the action to perform:")
    print("1. Generate shares")
    print("2. Reconstruct the secret")

    choice = int(input("Enter your choice (1 or 2): "))

    if choice == 1:
        k = int(input("Enter the minimum number of shares required to reconstruct the secret (k): "))
        n = int(input("Enter the total number of shares to generate (n): "))
        secret = int(input("Enter the secret to share: "))
        shares = generate_shares(k, n, secret)
        print("Shares have been generated:")
        for i, share in enumerate(shares):
            print(f"Share {i+1}: {share}")
    elif choice == 2:
        num_shares = int(input("Enter the number of available shares: "))
        shares = []
        for i in range(num_shares):
            x = int(input(f"Enter the value of x for share {i+1}: "))
            y = int(input(f"Enter the value of y for share {i+1}: "))
            shares.append((x, y))
        secret = reconstruct_secret(shares)
        print(f"The reconstructed secret is: {secret}")
    else:
        print("Invalid choice. Please enter 1 or 2.")

if __name__ == "__main__":
    main()
