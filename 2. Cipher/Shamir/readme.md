# Shamir's Secret Sharing Scheme

This Python script implements Shamir's Secret Sharing Scheme, a cryptographic algorithm that allows a secret to be divided into multiple shares, such that the secret can only be reconstructed when a sufficient number of shares are combined.

## Functions

### `generate_coefficients(k, secret)`
Generates random coefficients to create a polynomial of degree k, with the secret as the constant coefficient.

### `evaluate_polynomial(coefficients, x)`
Evaluates the polynomial defined by the coefficients at a given point x.

### `generate_shares(k, n, secret)`
Generates n shares by evaluating the polynomial at random x values using the generated coefficients.

### `reconstruct_secret(shares)`
Reconstructs the secret using Lagrange interpolation from a sufficient number of shares.

### `main()`
The main function where the user can choose to generate shares or reconstruct the secret.

## Usage

1. Run the script.
2. Choose whether to generate shares or reconstruct the secret.
   - To generate shares:
     - Enter the minimum number of shares required to reconstruct the secret (k).
     - Enter the total number of shares to generate (n).
     - Enter the secret to share.
     - Shares will be generated with random x values and printed.
   - To reconstruct the secret:
     - Enter the number of available shares.
     - Enter the x and y values for each share.
     - The script will reconstruct and print the secret.

## Example

```
Choose the action to perform:
1. Generate shares
2. Reconstruct the secret
Enter your choice (1 or 2): 1
Enter the minimum number of shares required to reconstruct the secret (k): 3
Enter the total number of shares to generate (n): 5
Enter the secret to share: 42
Shares have been generated:
Share 1: (773, 254702)
Share 2: (129, 53570)
Share 3: (165, 82562)
Share 4: (684, 211002)
Share 5: (320, 115522)

Choose the action to perform:
1. Generate shares
2. Reconstruct the secret
Enter your choice (1 or 2): 2
Enter the number of available shares: 3
Enter the value of x for share 1: 773
Enter the value of y for share 1: 254702
Enter the value of x for share 2: 129
Enter the value of y for share 2: 53570
Enter the value of x for share 3: 165
Enter the value of y for share 3: 82562
The reconstructed secret is: 42
```

Feel free to use this script to generate shares and reconstruct secrets using Shamir's Secret Sharing Scheme!