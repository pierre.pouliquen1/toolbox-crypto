def encrypt_round(key, block):
    w = [0] * 8
    for i in range(8):
        w[i] = block[i]
    
    for i in range(8):
        k = key[i % 10]
        w[0] = ((w[0] << 1) | (w[0] >> 7)) & 0xFF
        w[0] = (w[0] + w[1]) & 0xFF
        w[0] = (w[0] ^ k) & 0xFF
        w[1] = (w[1] - w[2]) & 0xFF
        w[1] = (w[1] ^ w[7]) & 0xFF
        w[2] = ((w[2] >> 1) | (w[2] << 7)) & 0xFF
        temp = w[3]
        w[3] = w[4]
        w[4] = w[5]
        w[5] = w[6]
        w[6] = temp
    
    return w

def decrypt_round(key, block):
    w = [0] * 8
    for i in range(8):
        w[i] = block[i]
    
    for i in range(8):
        temp = w[6]
        w[6] = w[5]
        w[5] = w[4]
        w[4] = w[3]
        w[3] = temp
        w[2] = ((w[2] << 1) | (w[2] >> 7)) & 0xFF
        w[1] = (w[1] ^ w[7]) & 0xFF
        w[1] = (w[1] + w[2]) & 0xFF
        w[0] = (w[0] ^ key[(7 - i) % 10]) & 0xFF
    
    return w

def skipjack_encrypt(key, plaintext):
    if len(plaintext) != 8:
        raise ValueError("Plaintext length must be 8 bytes")
    
    block = [0] * 8
    for i in range(8):
        block[i] = ord(plaintext[i])
    
    for i in range(2):
        block = encrypt_round(key, block)
    
    return bytes(block)

def skipjack_decrypt(key, ciphertext):
    if len(ciphertext) != 8:
        raise ValueError("Ciphertext length must be 8 bytes")
    
    block = [0] * 8
    for i in range(8):
        block[i] = ciphertext[i]
    
    for i in range(2):
        block = decrypt_round(key, block)
    
    return bytes(block)

def main():
    key = [0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09]

    while True:
        choice = input("Enter 'E' to encrypt, 'D' to decrypt, or 'Q' to quit: ").upper()
        if choice == 'E':
            plaintext = input("Enter the plaintext to encrypt (8 characters): ")
            if len(plaintext) != 8:
                print("Plaintext must be 8 characters long.")
                continue
            ciphertext = skipjack_encrypt(key, plaintext)
            print("Ciphertext:", ciphertext.hex())
        elif choice == 'D':
            ciphertext_hex = input("Enter the ciphertext to decrypt (16 hexadecimal characters): ")
            if len(ciphertext_hex) != 16:
                print("Ciphertext must be 16 hexadecimal characters.")
                continue
            try:
                ciphertext = bytes.fromhex(ciphertext_hex)
            except ValueError:
                print("Invalid hexadecimal string.")
                continue
            decrypted_plaintext = skipjack_decrypt(key, ciphertext)
            print("Decrypted plaintext:", decrypted_plaintext.decode())
        elif choice == 'Q':
            break
        else:
            print("Invalid choice. Please enter 'E', 'D', or 'Q'.")

if __name__ == "__main__":
    main()
