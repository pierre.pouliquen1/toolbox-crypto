# Skipjack Encryption and Decryption

This Python script implements the Skipjack cipher, a symmetric key block cipher designed for use in the Clipper chip. Skipjack operates on 64-bit blocks and uses an 80-bit key. It performs encryption and decryption rounds to process the input data.

## Functions

### `encrypt_round(key, block)`
Performs a single encryption round of the Skipjack algorithm.

### `decrypt_round(key, block)`
Performs a single decryption round of the Skipjack algorithm.

### `skipjack_encrypt(key, plaintext)`
Encrypts the plaintext using the Skipjack algorithm with the specified key.

### `skipjack_decrypt(key, ciphertext)`
Decrypts the ciphertext using the Skipjack algorithm with the specified key.

### `main()`
The main function where the user can choose whether to encrypt or decrypt text.

## Usage

1. Run the script.
2. Choose whether to encrypt or decrypt text.
   - If encrypting:
     - Enter the plaintext to encrypt (must be 8 characters).
   - If decrypting:
     - Enter the ciphertext to decrypt (must be 16 hexadecimal characters).
3. The encrypted or decrypted text will be printed.

## Example

```
Enter 'E' to encrypt, 'D' to decrypt, or 'Q' to quit: E
Enter the plaintext to encrypt (8 characters): ABCDEFGH
Ciphertext: 7d557f2cd9df03e8

Enter 'E' to encrypt, 'D' to decrypt, or 'Q' to quit: D
Enter the ciphertext to decrypt (16 hexadecimal characters): 7d557f2cd9df03e8
Decrypted plaintext: ABCDEFGH

Enter 'E' to encrypt, 'D' to decrypt, or 'Q' to quit: Q
```

Feel free to use this script to encrypt and decrypt data using the Skipjack cipher algorithm!