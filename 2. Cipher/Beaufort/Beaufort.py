def generate_tableau():
    alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    tableau = [[(alphabet[(j + i) % 26]) for j in range(26)] for i in range(26)]
    return tableau

def beaufort_encrypt(plaintext, key):
    tableau = generate_tableau()
    ciphertext = ""

    for p, k in zip(plaintext, key):
        col_index = ord(p) - ord('A')
        row_index = ord(k) - ord('A')
        ciphertext += tableau[row_index][col_index]

    return ciphertext

def beaufort_decrypt(ciphertext, key):
    tableau = generate_tableau()
    plaintext = ""

    for c, k in zip(ciphertext, key):
        col_index = ord(k) - ord('A')
        row_index = tableau[col_index].index(c)
        plaintext += chr(row_index + ord('A'))

    return plaintext

def main():
    choice = input("Enter 'E' to encrypt or 'D' to decrypt: ").upper()

    if choice == 'E':
        plaintext = input("Enter the plaintext: ").upper()
        key = input("Enter the key: ").upper()
        ciphertext = beaufort_encrypt(plaintext, key)
        print("Encrypted text:", ciphertext)
    elif choice == 'D':
        ciphertext = input("Enter the ciphertext: ").upper()
        key = input("Enter the key (must be same size as plain text): ").upper()
        plaintext = beaufort_decrypt(ciphertext, key)
        print("Decrypted text:", plaintext)
    else:
        print("Invalid choice. Please enter 'E' for encryption or 'D' for decryption.")

if __name__ == "__main__":
    main()
