# Beaufort Cipher Encryption and Decryption

This Python script implements the Beaufort Cipher for both encryption and decryption of text messages.

## Script Explanation

- `generate_tableau()`: Defines the function to generate the Beaufort tableau. It creates a 26x26 matrix where each row is a shifted alphabet according to the key.
- `beaufort_encrypt(plaintext, key)`: Defines the function to encrypt a plaintext using the Beaufort Cipher. It utilizes the generated tableau to perform encryption.
- `beaufort_decrypt(ciphertext, key)`: Defines the function to decrypt a ciphertext using the Beaufort Cipher. It utilizes the generated tableau to perform decryption.
- The `main()` function prompts the user to choose between encryption and decryption. It then prompts for the input text and key, and displays the encrypted or decrypted text accordingly.

## Usage

1. Run the script.
2. Enter 'E' to encrypt or 'D' to decrypt when prompted.
3. If encrypting:
   - Enter the plaintext when prompted.
   - Enter the key when prompted.
4. If decrypting:
   - Enter the ciphertext when prompted.
   - Enter the key (must be the same size as the plaintext) when prompted.
5. The script will display the encrypted or decrypted text accordingly.

## Note

- The Beaufort Cipher is a variant of the Vigenère Cipher where the plaintext is subtracted from the key in the tableau to obtain the ciphertext. It uses a tabula recta similar to the Vigenère Cipher for encryption and decryption.
- The script assumes that the input text and key contain only uppercase letters. Any other characters will not be encrypted or decrypted.