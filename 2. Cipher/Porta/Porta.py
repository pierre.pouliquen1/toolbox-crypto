alphabet = {
    "A": ("ABCDEFGHIJKLM", "NOPQRSTUVWXYZ"),
    "B": ("ABCDEFGHIJKLM", "NOPQRSTUVWXYZ"),
    "C": ("ABCDEFGHIJKLM", "ZNOPQRSTUVWXY"),
    "D": ("ABCDEFGHIJKLM", "ZNOPQRSTUVWXY"),
    "E": ("ABCDEFGHIJKLM", "YZNOPQRSTUVWX"),
    "F": ("ABCDEFGHIJKLM", "YZNOPQRSTUVWX"),
    "G": ("ABCDEFGHIJKLM", "XYZNOPQRSTUVW"),
    "H": ("ABCDEFGHIJKLM", "XYZNOPQRSTUVW"),
    "I": ("ABCDEFGHIJKLM", "WXYZNOPQRSTUV"),
    "J": ("ABCDEFGHIJKLM", "WXYZNOPQRSTUV"),
    "K": ("ABCDEFGHIJKLM", "VWXYZNOPQRSTU"),
    "L": ("ABCDEFGHIJKLM", "VWXYZNOPQRSTU"),
    "M": ("ABCDEFGHIJKLM", "UVWXYZNOPQRST"),
    "N": ("ABCDEFGHIJKLM", "UVWXYZNOPQRST"),
    "O": ("ABCDEFGHIJKLM", "TUVWXYZNOPQRS"),
    "P": ("ABCDEFGHIJKLM", "TUVWXYZNOPQRS"),
    "Q": ("ABCDEFGHIJKLM", "STUVWXYZNOPQR"),
    "R": ("ABCDEFGHIJKLM", "STUVWXYZNOPQR"),
    "S": ("ABCDEFGHIJKLM", "RSTUVWXYZNOPQ"),
    "T": ("ABCDEFGHIJKLM", "RSTUVWXYZNOPQ"),
    "U": ("ABCDEFGHIJKLM", "QRSTUVWXYZNOP"),
    "V": ("ABCDEFGHIJKLM", "QRSTUVWXYZNOP"),
    "W": ("ABCDEFGHIJKLM", "PQRSTUVWXYZNO"),
    "X": ("ABCDEFGHIJKLM", "PQRSTUVWXYZNO"),
    "Y": ("ABCDEFGHIJKLM", "OPQRSTUVWXYZN"),
    "Z": ("ABCDEFGHIJKLM", "OPQRSTUVWXYZN"),
}

def generate_table(key: str) -> list[tuple[str, str]]:
    return [alphabet[char] for char in key.upper()]

def encrypt(key: str, words: str) -> str:
    cipher = ""
    count = 0
    table = generate_table(key)
    
    for char in words.upper():
        cipher += transform(table[count], char)
        count = (count + 1) % len(table)
    
    return cipher

def decrypt(key: str, words: str) -> str:
    return encrypt(key, words)

def transform(table: tuple[str, str], char: str) -> str:
    row = 0 if char in table[0] else 1
    col = table[row].index(char.upper()) if row in {0, 1} else 0
    return table[1 - row][col] if row in {0, 1} else char


if __name__ == "__main__":
    choice = input("Enter 'E' to encrypt or 'D' to decrypt: ").strip().upper()

    if choice not in {'E', 'D'}:
        print("Invalid choice. Please enter 'E' or 'D'.")
    else:
        key = input("Enter key: ").strip()
        text = input(f"Enter text to {'encrypt' if choice == 'E' else 'decrypt'}: ").strip()

        if not key.isalpha() or not text.isalpha():
            print("Invalid input. Key and text must contain only alphabetic characters.")
        else:
            if choice == 'E':
                result = encrypt(key, text)
                print(f"Encrypted: {result}")
            else:
                result = decrypt(key, text)
                print(f"Decrypted with key: {result}")