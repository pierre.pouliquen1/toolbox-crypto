# Alphabetical Transposition Cipher

This Python script implements the Alphabetical Transposition cipher for encryption and decryption. In this cipher, each letter of the plaintext is substituted based on a key.

## Cipher Rules
The substitution is based on a key, which determines how each letter is transformed. The key consists of two strings, each containing part of the alphabet. The substitution is done according to the following rules:
- For each letter in the plaintext:
  - Determine which part of the key (first or second) contains the letter.
  - Find the index of the letter in that part of the key.
  - Substitute the letter with the letter at the same index in the other part of the key.

## Functions

### `generate_table(key: str) -> list[tuple[str, str]]`
Generates the transposition table based on the provided key.

### `encrypt(key: str, words: str) -> str`
Encrypts the input words using the Alphabetical Transposition cipher and the provided key.

### `decrypt(key: str, words: str) -> str`
Decrypts the input words using the Alphabetical Transposition cipher and the provided key.

### `transform(table: tuple[str, str], char: str) -> str`
Transforms a single character based on the transposition table.

## Usage

1. Run the script.
2. Enter 'E' to encrypt or 'D' to decrypt.
3. Enter the key (a string containing only alphabetic characters).
4. Enter the text to encrypt or decrypt (a string containing only alphabetic characters).
5. The encrypted or decrypted text will be displayed accordingly.

## Example

```
Enter 'E' to encrypt or 'D' to decrypt: E
Enter key: secret
Enter text to encrypt: hello
Encrypted: AXPPE

Enter 'E' to encrypt or 'D' to decrypt: D
Enter key: secret
Enter text to decrypt: AXPPE
Decrypted with key: HELLO
```

Feel free to use this script to encrypt and decrypt messages using the Alphabetical Transposition cipher!