**Easy** \
Caesar cipher (pay attention to spaces and accent) \


**Hard** \
Substitution (Make a menu with the choice of substitution by letters, groups of letters or random ASCII characters) \
Make a digram/trigram testing algorithm \
Make a French test algorithm with dictionary search (See for threading) \
Fitness function with several languages ​​and the possibility of importing a file \
Frequency analysis on a file \
Make one big program \
Vigenère decipherment \
Fitness function with several languages ​​and the possibility of importing a file \
Add AC intialisation\
Code one time pad \
Coding McEliece \



TO DO :

Anubis
ARIA
Blowfish
Cast5 (libraries exist)
Cast6 (libraries exist)
Khazad
Chacha
IDEA
RC2/4/5/6 (libraries exist)
Serpent
SHACAL2
Threefish
Threesquare
Twofish
Twosquare