
<div align="center">

![Crypto Toolbox Logo](4.%20Config/logocryptotoolbox.png){width=300 height=300px}
</div>
Welcome to the Crypto Toolbox project! This repository serves as a collection of Python code samples, focusing on encryption algorithms, algebraic concepts, and various tools relevant to cryptography. Whether you're a cryptography enthusiast, a student, or a developer looking to enhance your understanding of encryption techniques, this toolbox is here to assist you.

## Table of Contents

- [Introduction](#introduction)
- [Contents](#contents)
- [Getting Started](#getting-started)
- [Dependencies](#Dependencies)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Introduction

Crypto Toolbox is a curated collection of Python scripts designed to provide insights into encryption algorithms, algebraic foundations, and practical cryptography tools. Our aim is to create an educational and reference resource that aids both beginners and experienced developers in grasping the intricate world of cryptography.

## Contents

The repository is organized into the following sections:

1. **Encryption Algorithms:** Explore various encryption algorithms such as AES, RSA, and more. Each algorithm is accompanied by a sample script to demonstrate its implementation.

2. **Algebraic Concepts:** Delve into the mathematical foundations of cryptography, including modular arithmetic, prime number generation, and other algebraic principles.

3. **Cryptography Tools:** Discover an assortment of practical tools that assist in tasks like key generation, encryption/decryption, and cryptographic analysis.

## Getting Started

To begin using the Crypto Toolbox, follow these steps:

1. **Clone the Repository:** Start by cloning this Git repository using the following command:
   ```
   git clone https://gitlab.com/pierre.pouliquen1/toolbox-crypto.git
   ```

2. **Navigate to Directory:** Move into the repository directory:
   ```
   cd toolbox-crypto
   ```

3. **Explore the Content:** Browse through the directories to access various Python scripts and resources related to encryption algorithms, algebraic concepts, and cryptography tools.

## Dependencies

   No dependencies for now

## Usage

Each directory within the repository contains its own README with usage instructions for the specific code samples. Feel free to explore and modify the code to suit your needs. If you find a bug or have an improvement, please consider contributing (see [Contributing](#contributing)).

## Contributing

We welcome contributions to enhance the Crypto Toolbox project. If you've discovered a bug, have ideas for new features, or want to improve the existing code, follow these steps:

1. Fork the repository.
2. Create a new branch for your contribution:
   ```
   git checkout -b feature/your-feature-name
   ```
3. Make your modifications and improvements.
4. Commit and push your changes to your forked repository.
5. Create a pull request detailing your changes.

## License

This project is licensed under the [MIT License](LICENSE), which means you're free to use, modify, and distribute the code for educational and non-commercial purposes.

---

We hope the Crypto Toolbox helps you in your journey to understand and explore cryptography better. If you have any questions, ideas, or suggestions, please feel free to reach out to us or submit an issue on this repository. Happy coding and learning!

*Disclaimer: The Crypto Toolbox is intended for educational purposes only. Always follow best security practices when implementing cryptographic solutions in real-world applications.*

## Thanks to :

Cryptography icons created by juicy_fish "https://www.flaticon.com/free-icons/cryptography" title="cryptography icons" 

