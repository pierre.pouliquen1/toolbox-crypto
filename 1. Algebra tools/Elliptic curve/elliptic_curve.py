import numpy as np
import matplotlib.pyplot as plt

def elliptic_curve(x, a, b):
    return np.sqrt(x**3 + a*x + b)

def main():
    print("Elliptic Curve Plotter")
    a = float(input("Enter the 'a' coefficient: "))
    b = float(input("Enter the 'b' coefficient: "))
    
    x_values = np.linspace(-10, 10, 400)
    y_values_positive = elliptic_curve(x_values, a, b)
    y_values_negative = -y_values_positive
    
    plt.figure(figsize=(8, 6))
    plt.plot(x_values, y_values_positive, label='y^2 = x^3 + {}x + {}'.format(a, b))
    plt.plot(x_values, y_values_negative)
    plt.title("Elliptic Curve: y^2 = x^3 + {}x + {}".format(a, b))
    plt.xlabel('x')
    plt.ylabel('y')
    plt.axhline(0, color='black', linewidth=0.5, linestyle='--')
    plt.axvline(0, color='black', linewidth=0.5, linestyle='--')
    plt.grid(True, linestyle='--', alpha=0.7)
    plt.legend()
    plt.show()

if __name__ == "__main__":
    main()
