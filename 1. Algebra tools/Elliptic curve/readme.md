# Elliptic Curve Plotter

This Python script allows you to visualize elliptic curves defined by the equation `y^2 = x^3 + ax + b`. It utilizes NumPy for numerical computations and Matplotlib for plotting.

## Script Explanation

- `elliptic_curve(x, a, b)`: Defines the elliptic curve function.
- `main()`: The main function that prompts the user to input coefficients `a` and `b`, computes the corresponding y values, and plots the elliptic curve.
- The script plots both the positive and negative values of y to visualize the symmetric nature of elliptic curves.
- The plot includes labels, title, axis labels, grid lines, and a legend for clarity.

## Requirements

- Python 3.x
- NumPy
- Matplotlib

