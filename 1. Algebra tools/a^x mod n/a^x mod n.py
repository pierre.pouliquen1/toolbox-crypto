import string

print()
print("Welcome to this short code to calculate a^x mod n by breaking down the power")
a = int(input("Enter an integer 'a' as input: "))
x = int(input("Enter an integer 'x' as input: "))
n = int(input("Enter an integer 'n' as input: ")) 
print("We are going to find b such as " + str(a) + "^" + str(x) + " = b mod(" + str(n)+")")
print()
print("** Computing**")
m=1
result = 2
lpo2 = [1]
while(result < x and m <= 50):
    lpo2+= [result]
    m+=1
    result = 2**m
m-=1

# decomposition of x into factors of 2
sum_loop = 0
loop_iter = len(lpo2)
while loop_iter >0:
    loop_iter-=1
    if (sum_loop + lpo2[loop_iter]) > x:
        lpo2[loop_iter] = 0
    else:
        sum_loop+=lpo2[loop_iter]

sum_final = 0
string_final_raw = ""
string_final = ""
squared_a = [a]
for i in range(len(lpo2)):
    if(i != 0):
            squared_a+=[squared_a[i-1]**2]
    if(lpo2[i] !=0):
        if (sum_final==0):
            sum_final=squared_a[i]%n
        else:
            sum_final*=squared_a[i]%n
        string_final_raw+= str(a) + "^" + str(lpo2[i])
        string_final+=str(squared_a[i]%n)
        if(i != len(lpo2)-1):
            string_final_raw+="*"
            string_final+="*"
print()
print("Raw decomposition:", string_final_raw)
print("Decomposition:", string_final)
print("Final result:", sum_final%n)
      