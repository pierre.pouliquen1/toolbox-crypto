# Modular Exponentiation Breakdown

This Python script calculates \(a^x \mod n\) by breaking down the power \(x\) into factors of 2.

## Script Explanation

- The script prompts the user to input three integers: \(a\), \(x\), and \(n\), representing the base, exponent, and modulus, respectively.
- It then decomposes \(x\) into powers of 2 and performs modular exponentiation accordingly.
- The script prints the step-by-step breakdown of the modular exponentiation process, including the raw decomposition, the decomposition with base \(a\), and the final result.

## Requirements

- Python 3.x

## Usage

1. Make sure you have Python installed on your system.
2. Run the script.
3. Enter the value of \(a\) when prompted.
4. Enter the value of \(x\) when prompted.
5. Enter the value of \(n\) when prompted.
6. The script will calculate \(a^x \mod n\) and provide a breakdown of the computation steps.

## Note

This script provides a visual breakdown of the modular exponentiation process, which can be helpful for understanding how the computation is performed.