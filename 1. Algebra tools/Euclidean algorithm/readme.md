# Euclidean Algorithm

This Python script implements the Euclidean algorithm to find the greatest common divisor (GCD) of two integers `a` and `b`.

## Script Explanation

- `euclidean_algorithm(a, b)`: Defines the Euclidean algorithm function.
- `main()`: The main function that prompts the user to input two integers `a` and `b`, and then invokes the `euclidean_algorithm` function to compute their GCD.
- The script iteratively applies the Euclidean algorithm until `b` becomes zero, printing the quotient `q`, remainder `r`, and updating `a` and `b` accordingly.
- Once the algorithm terminates (when `b` becomes zero), it prints the GCD, which is the last non-zero value of `b`.

## Requirements

- Python 3.x

## Usage

1. Make sure you have Python installed on your system.
2. Run the script.
3. Enter the values of `a` and `b` when prompted.
4. The script will compute and print the GCD of the entered values.

## Note

This script is a simple implementation of the Euclidean algorithm and may not be optimized for extremely large inputs.