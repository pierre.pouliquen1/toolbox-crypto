def euclidean_algorithm(a, b):
    while b != 0:
        q = a // b  
        r = a % b   
        print(f"{a} = {b} * {q} + {r}")  
        a = b       
        b = r       
    print(f"The GCD is: {a}")  

def main():
    print("Euclidean Algorithm")
    a = int(input("Enter the value of a: ")) 
    b = int(input("Enter the value of b: "))  
    euclidean_algorithm(a, b)  

if __name__ == "__main__":
    main()  
