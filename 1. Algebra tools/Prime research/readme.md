# Basic Prime Checker

This Python script checks whether a given integer `n` is a prime number using a basic primality test.

## Script Explanation

- `basic_isPrime(n)`: Defines the function to check if `n` is a prime number. It iterates through integers up to the square root of `n` and checks if any of them divide `n` evenly. If a divisor is found, the function returns `False`, indicating that `n` is not prime. Otherwise, it returns `True`.
- The script prompts the user to enter an integer `n`, and then prints `True` if `n` is prime and `False` otherwise.

## Requirements

- Python 3.x

## Usage

1. Make sure you have Python installed on your system.
2. Run the script.
3. Enter an integer `n` when prompted.
4. The script will check if `n` is a prime number and print the result (`True` or `False`).

## Note

This script is suitable for checking primality of small integers but may not be efficient for very large numbers. It uses a basic primality test that checks divisibility up to the square root of `n`.