def basic_isPrime(n):
    root = n**(1/2)
    iterator = 1
    multiple_list = []
    result = True
    while (iterator <= root):
        iterator+=1
        if not(iterator in multiple_list):
            if(n%iterator==0):
                result = False
                break
            else:
                k=1
                while k*iterator <= root:
                    
                    multiple_list+=[k*iterator]
                    k+=1
    return result

n = int(input("Enter an integer 'n' as input: ")) 
print(basic_isPrime(n))