from tabulate import tabulate

def multiplication_table_modulo(n, modulo):
    table_data = []
    for i in range(1, n+1):
        row = []
        for j in range(1, n+1):
            result = (i * j) % modulo
            row.append(f"{i}*{j} = {result}")
        table_data.append(row)
    
    headers = [f"Mod {modulo}"]
    for i in range(1, n+1):
        headers.append(f"{i}")
    
    table = tabulate(table_data, headers=headers, tablefmt="grid")
    print(table)

def main():
    n = int(input("Enter the value of n: "))
    modulo = int(input("Enter the value of modulo: "))
    multiplication_table_modulo(n, modulo)

if __name__ == "__main__":
    main()
