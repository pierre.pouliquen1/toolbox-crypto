def chinese_remainder_theorem(mod, rem):
    k = len(mod)
    x = 1
    max_iter = 1000
    while x < max_iter:
        j = 0
        while j < k:
            if rem[j] != x % mod[j]:
                break
            j+= 1
        if j == k:
            return x
        x+= 1
    return "error"

if __name__ == "__main__":
    mod = []
    rem = []

    k = int(input("Enter the number of equations: "))
    for i in range(k):
        n = int(input(f"Enter the value of mod[{i}]: "))
        r = int(input(f"Enter the value of rem[{i}]: "))
        mod.append(n)
        rem.append(r)
        
    print("n is equal to", chinese_remainder_theorem(mod, rem))
