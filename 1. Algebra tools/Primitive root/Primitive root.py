def gcd(a, b):
    while b:
        a, b = b, a % b
    return a

def is_primitive_root(a, p):
    if gcd(a, p) != 1:
        return False

    totient = p - 1
    factors = [1]
    for i in range(2, totient + 1):
        if gcd(i, totient) == 1:
            factors.append(i)

    for factor in factors:
        if pow(a, factor, p) == 1:
            return False

    return True

def main():
    a = int(input("Enter the value of 'a': "))
    p = int(input("Enter the value of 'p': "))

    if is_primitive_root(a, p):
        print(f"{a} is a primitive root modulo {p}.")
    else:
        print(f"{a} is not a primitive root modulo {p}.")

if __name__ == "__main__":
    main()
