# Primitive Root Checker

This Python script checks whether a given integer `a` is a primitive root modulo a prime number `p`.

## Script Explanation

- `gcd(a, b)`: Defines the function to compute the greatest common divisor (GCD) of two integers `a` and `b` using the Euclidean algorithm.
- `is_primitive_root(a, p)`: Defines the function to check if `a` is a primitive root modulo `p`. It first checks if `a` and `p` are coprime (i.e., their GCD is 1). Then, it computes the totient function of `p` (denoted as `totient`) and generates a list of all numbers coprime to `p`. For each coprime factor, it checks if `a` raised to the power of that factor modulo `p` is not equal to 1. If such a factor is found for which the condition is not satisfied, it returns `False`, indicating that `a` is not a primitive root modulo `p`. If no such factor is found, it returns `True`, indicating that `a` is a primitive root modulo `p`.
- `main()`: The main function that prompts the user to input the values of `a` and `p`. It then invokes the `is_primitive_root` function to check if `a` is a primitive root modulo `p` and prints the result accordingly.

## Requirements

- Python 3.x

## Usage

1. Make sure you have Python installed on your system.
2. Run the script.
3. Enter the value of `a` when prompted.
4. Enter the value of `p` when prompted.
5. The script will check if `a` is a primitive root modulo `p` and print the result (`True` or `False`).

## Note

This script assumes that `p` is a prime number. Additionally, it may not be optimized for very large values of `p`.