# GCD and LCM Calculator

This Python script allows you to compute the greatest common divisor (GCD) and the least common multiple (LCM) of two integers.

## Script Explanation

- `gcd(a, b)`: Defines the function to compute the GCD of two integers `a` and `b` using the Euclidean algorithm.
- `lcm(a, b)`: Defines the function to compute the LCM of two integers `a` and `b` using the formula `(a * b) / gcd(a, b)`.
- `main()`: The main function that prompts the user to choose between computing the GCD or the LCM. Depending on the choice, it prompts the user to input two integers and then computes and prints either the GCD or the LCM.
- The script handles invalid choices by informing the user to input either 1 or 2.

## Requirements

- Python 3.x

## Usage

1. Make sure you have Python installed on your system.
2. Run the script.
3. Choose an option:
   - Enter `1` to compute the GCD.
   - Enter `2` to compute the LCM.
4. If you chose option `1`:
   - Enter the first number.
   - Enter the second number.
   - The script will compute and print the GCD of the entered numbers.
5. If you chose option `2`:
   - Enter the first number.
   - Enter the second number.
   - The script will compute and print the LCM of the entered numbers.
6. If you enter an invalid choice (neither `1` nor `2`), the script will prompt you to enter a valid choice.

## Note

This script assumes that the user will input valid integers for computation.