def gcd(a, b):
    while b:
        a, b = b, a % b
    return a

def lcm(a, b):
    return (a * b) // gcd(a, b)

def main():
    print("Choose an option:")
    print("1. Compute GCD")
    print("2. Compute LCM")
    choice = int(input("Enter your choice (1/2): "))

    if choice == 1:
        num1 = int(input("Enter the first number: "))
        num2 = int(input("Enter the second number: "))
        result = gcd(num1, num2)
        print(f"The GCD of {num1} and {num2} is: {result}")
    elif choice == 2:
        num1 = int(input("Enter the first number: "))
        num2 = int(input("Enter the second number: "))
        result = lcm(num1, num2)
        print(f"The LCM of {num1} and {num2} is: {result}")
    else:
        print("Invalid choice. Please enter 1 or 2.")

if __name__ == "__main__":
    main()
