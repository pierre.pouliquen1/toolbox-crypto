# Lehmer Primality Test

This Python script implements the Lehmer primality test to check if a number of the form \( N = 2^x \cdot p^y \cdot q^z + 1 \) is likely to be prime, where \( p \) and \( q \) are prime numbers.

## Script Explanation

- `lehmer(p, q, x, y, z, max_value)`: Defines the Lehmer primality test function. It calculates \( N = 2^x \cdot p^y \cdot q^z + 1 \) and then iterates through integers from 2 up to `max_value` to check if certain conditions are satisfied. If the conditions are met, it returns `True`, indicating that \( N \) is likely to be prime.
- `main()`: The main function that prompts the user to input the values of \( p \), \( q \), \( x \), \( y \), \( z \), and `max_value`. It then invokes the `lehmer` function to check if the conditions are satisfied for the given parameters. It also gives the option to try different values of \( x \), \( y \), and \( z \) to explore further.
- The script prints whether the conditions are satisfied for the given parameters and asks the user if they want to explore more combinations of \( x \), \( y \), and \( z \).

## Requirements

- Python 3.x

## Usage

1. Make sure you have Python installed on your system.
2. Run the script.
3. Enter the values of \( p \), \( q \), \( x \), \( y \), \( z \), and `max_value`.
4. The script will calculate \( N \) and check if the conditions for primality are satisfied.
5. It will then ask if you want to explore more combinations of \( x \), \( y \), and \( z \). Enter `Y` for yes or `N` for no.
6. If you choose to explore more combinations, the script will print the values of \( N \) for which the conditions are satisfied for various combinations of \( x \), \( y \), and \( z \).

## Note

This script is intended for educational purposes and may not be optimized for large inputs. Additionally, it relies on the Lehmer primality test, which is probabilistic and does not guarantee primality.