def lehmer(p, q, x, y, z, max_value):
    N = 2**x * p**y * q**z + 1

    for a in range(2, max_value):
        if pow(a, ((N - 1) // 2), N) != (N - 1) % N:
            continue
        elif pow(a, (N - 1) // p, N) == 1:
            continue
        elif pow(a, (N - 1) // q, N) == 1:
            continue
        else:
            return N, True

    return N, False

def main():
    p = int(input("Enter the value of p: "))
    q = int(input("Enter the value of q: "))
    x = int(input("Enter the value of x: "))
    y = int(input("Enter the value of y: "))
    z = int(input("Enter the value of z: "))
    max_value = int(input("Enter the max value: "))
    equation = f"2^{x} * {p}^{y} * {q}^{z} + 1"
    print("Trying to find if "+ str(equation) + " likely to be prime" )
    
    N, result = lehmer(p, q, x, y, z, max_value)
    
    if result:
        print(f"The conditions are satisfied for N = {N}.")
    else:
        print(f"The conditions are not satisfied for N = {N}.")
    
    question = str(input("Do you want to try with many value for x,y and z ? (Y)es or (N)o : "))
    if (question == "Y"):
        for x in range(21):
            for y in range(51):
                for z in range(51):
                    N, result = lehmer(p, q, x, y, z, max_value)
                    if result:
                        print(f"The conditions are satisfied for N = {N}.")

if __name__ == "__main__":
    main()
