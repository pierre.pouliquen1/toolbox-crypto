
# Sieve of Eratosthenes Algorithm

This repository demonstrates the Sieve of Eratosthenes algorithm, a method for finding all prime numbers up to a given limit. The algorithm works by iteratively marking the multiples of each prime number as non-prime, leaving only the prime numbers behind.

## Steps

1. The algorithm initializes a list of boolean values representing whether each number is prime or not. It sets the values for 0 and 1 to False since they are not prime.

2. It then iterates over numbers starting from 2 up to the square root of the given limit. For each number:

   a. If the number is marked as prime, it identifies its multiples by starting from its square and stepping by the number itself. These multiples are not prime numbers, so they are marked as non-prime by changing their boolean value in the list.

   b. The algorithm prints the list of multiples and the prime number responsible for marking them as non-prime.

3. After iterating through all numbers, the remaining numbers marked as prime in the list are the prime numbers up to the given limit.

## Notes

- The Sieve of Eratosthenes is an efficient way to find prime numbers within a certain range. Its time complexity is approximately O(n log log n), making it much faster than testing each number individually for primality.
- This implementation includes explanatory print statements to guide you through the algorithm's execution and help you understand its steps.

