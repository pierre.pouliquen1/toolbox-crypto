def sieve_of_eratosthenes(limit):
    primes = [True] * (limit + 1)
    primes[0] = primes[1] = False

    for num in range(2, int(limit ** 0.5) + 1):
        if primes[num]:
            multiples = [multiple for multiple in range(num * num, limit + 1, num)]
            primes[multiples[0]:limit+1:num] = [False] * len(multiples)
            print(f"Multiples of {num}: {multiples}")
    
    prime_numbers = [num for num, is_prime in enumerate(primes) if is_prime][2:]  # Exclude 0 and 1

    return prime_numbers

def main():
    print("Welcome to the Sieve of Eratosthenes demonstration!")
    upper_limit = int(input("Enter an upper limit to find prime numbers up to: "))
    
    prime_numbers = sieve_of_eratosthenes(upper_limit)
    
    print("\nPrime numbers up to", upper_limit, "are:", prime_numbers)

if __name__ == "__main__":
    main()
