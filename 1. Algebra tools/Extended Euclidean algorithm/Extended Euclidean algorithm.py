def extended_gcd(a, b):
    if a == 0:
        return b, 0, 1
    else:
        gcd, x, y = extended_gcd(b % a, a)
        return gcd, y - (b // a) * x, x

def mod_inverse(a, n):
    gcd, x, _ = extended_gcd(a, n)
    if gcd != 1:
        raise ValueError("Modular inverse does not exist")
    return x % n

def main():
    try:
        a = int(input("Enter the value of 'a': "))
        n = int(input("Enter the value of 'n': "))
        
        if n <= 0:
            raise ValueError("n must be a positive integer")
        
        inverse = mod_inverse(a, n)
        print(f"The modular inverse of {a} modulo {n} is: {inverse}")
    except ValueError as e:
        print("Error:", e)

if __name__ == "__main__":
    main()
