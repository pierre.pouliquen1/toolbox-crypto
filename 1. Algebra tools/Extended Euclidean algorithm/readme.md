# Extended Euclidean Algorithm for Modular Inverse

This Python script calculates the modular inverse of an integer `a` modulo `n` using the extended Euclidean algorithm.

## Script Explanation

- `extended_gcd(a, b)`: Defines the extended Euclidean algorithm function. It returns the greatest common divisor (GCD) of `a` and `b`, along with integers `x` and `y` such that `ax + by = gcd(a, b)`.
- `mod_inverse(a, n)`: Calculates the modular inverse of `a` modulo `n` using the extended Euclidean algorithm. It first computes the GCD of `a` and `n`, and then checks if the GCD is 1 (indicating that `a` and `n` are coprime). If the GCD is not 1, it raises a ValueError since the modular inverse does not exist. Otherwise, it returns the modular inverse of `a` modulo `n`.
- `main()`: The main function that prompts the user to input the values of `a` and `n`. It then invokes the `mod_inverse` function to compute the modular inverse of `a` modulo `n` and prints the result. It handles input validation and potential errors using a try-except block.

## Requirements

- Python 3.x

## Usage

1. Make sure you have Python installed on your system.
2. Run the script.
3. Enter the value of `a`.
4. Enter the value of `n`.
5. The script will compute and print the modular inverse of `a` modulo `n` if it exists. If `n` is not a positive integer, it will raise a ValueError.

## Note

This script assumes that `n` is a positive integer.
